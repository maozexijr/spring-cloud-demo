# Spring Cloud Gateway



### 1. Gateway

- Spring Cloud Gateway和Netflix Zuul的比较：

（1）Zuul停更，线程阻塞

（2）Gateway基于Spring Boot 2.x、Spring WebFlux和Project Reactor，支持异步非阻塞API，性能体现更优；

- Gateway的特点：

（1）支持动态路由（Route，正则匹配、服务发现）

（2）支持断言（Predicate，条件拦截）

（3）支持过滤器（Filter，响应前后的请求再封装）

（4）集成Hystrix（服务降级、熔断、限流）

（5）支持WebSocket（支持后端向前端的消息推送）

（6）支持异步非阻塞API，性能体现更优

- Gateway的核心概念：

（1）路由器（Route，请求转发）

（2）断言（Predicate，条件拦截）

（3）过滤器（Filter，响应前后的请求再封装）



### 2. 搭建Consul服务端

- 下载

```bash
# https://www.consul.io/downloads
```

- 安装与启动

```bash
# 双击安装
# 开发模式启动（程序文件所在路径下执行）
$ consul agent -dev
```

- 访问

```bash
# http://localhost:8500
# 免密登录
```



### 3. 搭建Consul客户端

##### （1）搭建2个业务实例 Provider

- 建立 `gateway-provider-2301` 子Module

- 在子pom中追加

```xml
<!-- Consul -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-consul-discovery</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableDiscoveryClient` 注解

```java
@SpringBootApplication
@EnableDiscoveryClient
public class GatewayProvider2301 {
    public static void main(String[] args) {
        SpringApplication.run(GatewayProvider2301.class, args);
    }
}
```

- 在application.yml追加配置

```yml
server:
  port: 2301

spring:
  application:
    name: gateway-provider-service
  cloud:
    consul:
      host: localhost
      port: 8500
      discovery:
        service-name: ${spring.application.name}
```

- 编写Controller类

```java
@RestController
@Slf4j
public class ProviderController {

    @Value("${server.port}")
    private String serverPort;

    @GetMapping(value = "/fixed/product/get/{id}")
    public Results fixedGet(@PathVariable("id") Long id) {
        return new Results(200, "The service is found using fixed routing", String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @GetMapping(value = "/dynamic/product/get")
    public Results dynamicGet() {
        return new Results(200, "The service is found using dynamic routing", String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @GetMapping(value = "/locator/product/get")
    public Results locatorGet() {
        return new Results(200, "The service is found using locator routing", String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @GetMapping(value = "/after2020/product/get")
    public Results after2020() {
        return new Results(200, "After=2020-01-01T00:00:00.000+08:00[Asia/Shanghai]", String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @GetMapping(value = "/nameAz/product/get")
    public Results nameAz(@RequestParam(name = "name", required = false) String name) {
        return new Results(200, "Query=name,[A-Za-z]{3}", String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @PostMapping(value = "/consumer/prefix/product/post")
    public Results byPrefix() {
        return new Results(200, "PrefixPath=/consumer", String.format("SUCCESS(PORT=%s)", serverPort));
    }

}
```

- 同样方法创建 `gateway-provider-2302`



##### （2）搭建1个 Gateway Middleware

- 建立 `gateway-middleware-2201` 子Module

- 在子pom中追加

```xml
<!-- Gateway -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-gateway</artifactId>
</dependency>

<!-- Consul -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-consul-discovery</artifactId>
</dependency>

<!-- Don't import spring-boot-starter-web -->
```

- 在SpringBoot启动类追加 `@EnableDiscoveryClient` 注解

```java
@SpringBootApplication
@EnableDiscoveryClient
public class GatewayMiddleware2201 {
    public static void main(String[] args) {
        SpringApplication.run(GatewayMiddleware2201.class, args);
    }
}
```

- 在application.yml追加配置

```yml
server:
  port: 2201

spring:
  application:
    name: gateway-middleware-service
  cloud:
    consul:
      host: localhost
      port: 8500
      discovery:
        service-name: ${spring.application.name}

    gateway:
      discovery:
        locator:
          enabled: true # 对动态路由的支持
      routes:
        - id: route_fixed # 随意不重复
          uri: http://localhost:2301 # 固定路由
          predicates:
            - Path=/fixed/product/get/** # 用**作通配符
            # curl http://localhost:2201/fixed/product/get/1?token=xxx

        - id: route_dynamic # 随意不重复
          uri: lb://gateway-provider-service # 动态路由（lb: LoadBalance）
          predicates:
            - Path=/dynamic/product/get
            # curl http://localhost:2201/dynamic/product/get?token=xxx

        - id: route_after
          uri: lb://gateway-provider-service
          predicates:
            - Path=/after2020/product/get
            - After=2020-01-01T00:00:00.000+08:00[Asia/Shanghai] # 活动开始
            # curl http://localhost:2201/after2020/product/get?token=xxx
          # - Before=2099-12-31T23:59:59.999+08:00[Asia/Shanghai] # 到期之前
          # - Between=2020-01-01T00:00:00.000+08:00[Asia/Shanghai],2099-12-31T23:59:59.999+08:00[Asia/Shanghai] # 时间区间

        - id: route_query # 随意不重复
          uri: lb://gateway-provider-service
          predicates:
            - Path=/nameAz/product/get
            - Method=GET
            - Query=name,[A-Za-z]{3}
            # curl http://localhost:2201/nameAz/product/get?name=xxx&token=xxx
          # - Cookie=chocolate,ch.p
          # - Header=X-Request-Id,\d+
          # - Host=**.alibaba.com,**.spring.io

        - id: route_filters # 随意不重复
          uri: lb://gateway-provider-service
          predicates:
            - Path=/prefix/product/post
            - Method=POST
          filters:
            - PrefixPath=/consumer
            # curl -x POST http://localhost:2201/prefix/product/post?token=xxx
            # 官方默认支持30多中过滤器
```

- 编写自定义路由
```java
@Configuration
public class MyRouteLocator {
    @Bean
    public RouteLocator builder(RouteLocatorBuilder locator) {
        RouteLocatorBuilder.Builder routes = locator.routes();
        routes.route("route_locator",
                r -> r.path("/locator/product/get")
                        .uri("http://localhost:2302")).build();
        // curl http://localhost:2201/locator/product/get?token=xxx
        return routes.build();
    }
}
```

- 编写自定义过滤器

```java
@Component
@Slf4j
public class MyGlobalFilter implements GlobalFilter, Ordered {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        String token = exchange.getRequest().getQueryParams().getFirst("token");
        if (token == null) {
            log.error("NO HAVE token o(╥﹏╥)o");
            exchange.getResponse().setStatusCode(HttpStatus.NOT_ACCEPTABLE);
            return exchange.getResponse().setComplete();
        }
        log.info("HAVE token 哈哈(づ￣ 3￣)づΨ(￣∀￣)Ψ(〃￣︶￣)人(￣︶￣〃)");
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        //
        return 0;
    }
}
```



### 4. 逐一启动各微服务

```bash
# 逐一启动 consul-server-8500.bat（注册中心）、gateway-provider-2301（微服务实例 生产者）、gateway-provider-2302（微服务实例 生产者）、gateway-middleware-2201（网关中间件）
```



### 5. 测试用例

```bash
# 测试静态路由（指定响应服务器）
$ curl http://localhost:2201/fixed/product/get/1
> 
$ curl http://localhost:2201/fixed/product/get/1?token=xxx
> {"code":200,"data":"Fixed routing","message":"SUCCESS(PORT=2301)"}

# 测试动态路由（依赖服务发现作动态匹配）
$ curl http://localhost:2201/dynamic/product/get?token=xxx
> {"code":200,"data":"Dynamic routing","message":"SUCCESS(PORT=2301)"}
$ curl http://localhost:2201/dynamic/product/get?token=xxx
> {"code":200,"data":"Dynamic routing","message":"SUCCESS(PORT=2302)"}

# 测试时间类断言（Predicate）
$ curl http://localhost:2201/after2020/product/get?token=xxx
> {"code":200,"data":"After=2020-01-01T00:00:00.000+08:00[Asia/Shanghai]","message":"SUCCESS(PORT=2301)"}
# 系统时间修改到2020年前
$ curl http://localhost:2201/after2020/product/get?token=xxx
> There was an unexpected error (type=Not Found, status=404).
> org.springframework.web.server.ResponseStatusException: 404 NOT_FOUND

# 测试HTTP类断言
$ curl http://localhost:2201/nameAz/product/get?name=abC&token=xxx
> {"code":200,"data":"Query=name,[A-Za-z]{3}","message":"SUCCESS(PORT=2302)"}
$ curl http://localhost:2201/nameAz/product/get?name=a2c&token=xxx
> There was an unexpected error (type=Not Found, status=404).
> org.springframework.web.server.ResponseStatusException: 404 NOT_FOUND

# 测试过滤器
$ curl -x POST http://localhost:2201/prefix/product/post?token=xxx
> {"code":200,"data":"PrefixPath=/consumer","message":"SUCCESS(PORT=2301)"}
# 会被转发成 http://localhost:2301/consumer/prefix/product/post
```



### 6. Gateway路由

- 静态路由配置

```yml
# 配置gateway-middleware-2201的application.yml
spring:
  application:
    name: gateway-middleware-service
    gateway:
      routes:
        - id: route_fixed # 随意不重复
          uri: http://localhost:2301 # 固定路由
          predicates:
            - Path=/fixed/product/get/** # 用**作通配符
            # curl http://localhost:2201/fixed/product/get/1?token=xxx
```

- 动态路由配置

```yml
# 配置gateway-middleware-2201的application.yml
# “动态”指的是通过注册发现，动态的匹配Provider
spring:
  application:
    name: gateway-middleware-service
    gateway:
      discovery:
        locator:
          enabled: true # 对动态路由的支持
      routes:
        - id: route_dynamic # 随意不重复
          uri: lb://gateway-provider-service # 动态路由（lb: LoadBalance）
          predicates:
            - Path=/dynamic/product/get
            # curl http://localhost:2201/dynamic/product/get?token=xxx
```

- Java路由实现

```java
// 添加gateway-middleware-2201的配置类
@Configuration
public class MyRouteLocator {
    @Bean
    public RouteLocator builder(RouteLocatorBuilder locator) {
        RouteLocatorBuilder.Builder routes = locator.routes();
        routes.route("route_locator",
                r -> r.path("/locator/product/get")
                        .uri("http://localhost:2302")).build();
        // curl http://localhost:2201/locator/product/get?token=xxx
        return routes.build();
    }
}
```



### 7. Gateway断言

  Spring Cloud Gateway 默认提供了 After、Before、Between、Path、Method、Query、Cookie、Header、Host等10多个断言

```yml
# 配置gateway-middleware-2201的application.yml
spring:
  application:
    name: gateway-middleware-service
    gateway:
      discovery:
        locator:
          enabled: true # 对动态路由的支持
      routes:
        - id: route_after
          uri: lb://gateway-provider-service
          predicates:
            - Path=/after2020/product/get
            - After=2020-01-01T00:00:00.000+08:00[Asia/Shanghai] # 活动开始
            # curl http://localhost:2201/after2020/product/get?token=xxx
          # - Before=2099-12-31T23:59:59.999+08:00[Asia/Shanghai] # 到期之前
          # - Between=2020-01-01T00:00:00.000+08:00[Asia/Shanghai],2099-12-31T23:59:59.999+08:00[Asia/Shanghai] # 时间区间

        - id: route_query
          uri: lb://gateway-provider-service
          predicates:
            - Path=/nameAz/product/get
            - Method=GET
            - Query=name,[A-Za-z]{3}
            # curl http://localhost:2201/nameAz/product/get?name=xxx&token=xxx
          # - Cookie=chocolate,ch.p
          # - Header=X-Request-Id,\d+
          # - Host=**.alibaba.com,**.spring.io
```



### 8. Gateway过滤器

- 配置使用

  Spring Cloud Gateway 默认提供了30多个过滤器，详情查看官方文档

```yml
# 配置gateway-middleware-2201的application.yml
spring:
  application:
    name: gateway-middleware-service
    gateway:
      discovery:
        locator:
          enabled: true # 对动态路由的支持
      routes:
        - id: route_filters
          uri: lb://gateway-provider-service
          predicates:
            - Path=/prefix/product/post
            - Method=POST
          filters:
            - PrefixPath=/consumer
            # curl -x POST http://localhost:2201/prefix/product/post?token=xxx
            # 官方默认支持30多种过滤器

```


- Java过滤器实现

```java
@Component
@Slf4j
public class MyGlobalFilter implements GlobalFilter, Ordered {

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        String token = exchange.getRequest().getQueryParams().getFirst("token");
        if (token == null) {
            log.error("NO HAVE token o(╥﹏╥)o");
            exchange.getResponse().setStatusCode(HttpStatus.NOT_ACCEPTABLE);
            return exchange.getResponse().setComplete();
        }
        log.info("HAVE token 哈哈(づ￣ 3￣)づΨ(￣∀￣)Ψ(〃￣︶￣)人(￣︶￣〃)");
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        //
        return 0;
    }
}
```