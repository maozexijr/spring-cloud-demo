package com.alibaba.cloudy.controller;

import com.alibaba.cloudy.entities.Results;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;


@RestController
@Slf4j
public class ProviderController {

    @Value("${server.port}")
    private String serverPort;

    @GetMapping(value = "/fixed/product/get/{id}")
    public Results fixedGet(@PathVariable("id") Long id) {
        // 静态路由（Route）
        return new Results(200, "Fixed routing", String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @GetMapping(value = "/dynamic/product/get")
    public Results dynamicGet() {
        // 动态路由（Route）
        return new Results(200, "Dynamic routing", String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @GetMapping(value = "/locator/product/get")
    public Results locatorGet() {
        // 自定义路由(Route)
        return new Results(200, "Custom routing", String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @GetMapping(value = "/after2020/product/get")
    public Results after2020() {
        // 断言（Predicate） 3个时间断言：After、Before、Between
        return new Results(200, "After=2020-01-01T00:00:00.000+08:00[Asia/Shanghai]", String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @GetMapping(value = "/nameAz/product/get")
    public Results nameAz(@RequestParam(name = "name", required = false) String name) {
        // 断言（Predicate） 8个HTTP断言：Path、Method、Query、Cookie、Header、Host等
        return new Results(200, "Query=name,[A-Za-z]{3}", String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @PostMapping(value = "/consumer/prefix/product/post")
    public Results byPrefix() {
        // 过滤器（Filter） 30多个
        return new Results(200, "PrefixPath=/consumer", String.format("SUCCESS(PORT=%s)", serverPort));
    }

}
