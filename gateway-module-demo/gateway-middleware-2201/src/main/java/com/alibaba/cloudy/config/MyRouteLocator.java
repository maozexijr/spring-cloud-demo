package com.alibaba.cloudy.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyRouteLocator {

    @Bean
    public RouteLocator builder(RouteLocatorBuilder locator) {
        RouteLocatorBuilder.Builder routes = locator.routes();
        routes.route("route_locator",
                r -> r.path("/locator/product/get")
                        .uri("http://localhost:2302")).build();
        // curl http://localhost:2201/locator/product/get?token=xxx
        return routes.build();
    }

}
