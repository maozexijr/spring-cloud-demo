package com.alibaba.cloudy.service;

import com.alibaba.cloudy.entities.Results;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient(value = "HYSTRIX-PROVIDER-SERVICE")
public interface ProductFeignService {

    @GetMapping(value = "/product/timeout/{seconds}")
    public Results<String> timeout(@PathVariable("seconds") Long seconds);

    @GetMapping(value = "/product/error/{flag}")
    public Results<String> error(@PathVariable("flag") Integer flag);

    @GetMapping(value = "/product/toomany")
    public Results<String> toomany();

    @GetMapping(value = "/product/normal")
    public Results<String> normal();

}
