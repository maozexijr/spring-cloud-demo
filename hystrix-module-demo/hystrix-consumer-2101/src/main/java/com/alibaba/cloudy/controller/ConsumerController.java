package com.alibaba.cloudy.controller;

import com.alibaba.cloudy.entities.Results;
import com.alibaba.cloudy.service.ProductFeignService;
import com.alibaba.cloudy.service.ProductHystrixService;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@RequestMapping(value = "/consumer")
@RestController
@Slf4j
@DefaultProperties(defaultFallback = "defaultFallback")
// Declare a default fallback method inside the class
public class ConsumerController {

    @Resource
    private ProductFeignService productFeignService;

    @Resource
    private ProductHystrixService productHystrixService;

    @Resource
    private DiscoveryClient discoveryClient;

    @GetMapping(value = "/discovery")
    public Results discovery() {
        List<String> infos = new ArrayList<>();

        List<String> services = discoveryClient.getServices();
        for (String service : services) {
            List<ServiceInstance> instances = discoveryClient.getInstances(service);
            for (ServiceInstance instance : instances) {
                infos.add(instance.getServiceId() + " " + instance.getUri());
            }
        }
        return new Results(200, infos, "SUCCESS");
    }

    /**
     * Responds to data requests normally
     */
    @GetMapping("/product/normal")
    public Results normal() {
        return productFeignService.normal();
    }

    /**
     * (1) Specify service fallback to the method
     */
    @GetMapping("/product/timeout/{seconds}")
    @HystrixCommand(fallbackMethod = "fallBackForTimeout", commandProperties = {
            @HystrixProperty(name = "execution.timeout.enabled", value = "true"),
            // Service fallback is triggered when this response time is exceeded
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "18000")
    })
    public Results timeout(@PathVariable("seconds") Long seconds) {
        return productFeignService.timeout(seconds);
    }

    public Results fallBackForTimeout(@PathVariable("seconds") Long seconds) {
        return new Results(500, "Consumer is fallback", "FAIL");
    }

    /**
     * (2) Use the default fallback method defined within the class
     */
    @GetMapping("/product/default")
    @HystrixCommand
    public Results defaults() {
        int a = 1 / 0;
        return new Results(200, "Consumer is running", "SUCCESS");
    }

    public Results defaultFallback() {
        //
        return new Results(500, "Default fallback", "FAIL");
    }

    /**
     * (3) Use the FeignClient implementation class to do service fallback
     */
    @GetMapping("/product/override")
    public Results override() {
        return productHystrixService.override();
    }

    /**
     * 服务熔断：10s内5次访问3次失败就可以引发服务熔断（发生熔断后的5s自动恢复）
     */
    @GetMapping(value = "/product/error/{flag}")
    public Results<String> error(@PathVariable("flag") Integer flag) {
        return productFeignService.error(flag);
    }

    /**
     * 服务限流
     */
    @GetMapping("/product/toomany/{times}")
    public Results toomany(@PathVariable("times") Integer times) {
        times = null == times ? 1000 : times;
        while (times > 0) {
            productFeignService.toomany();
            times--;
        }
        return new Results(200, null, "SUCCESS");
    }

}
