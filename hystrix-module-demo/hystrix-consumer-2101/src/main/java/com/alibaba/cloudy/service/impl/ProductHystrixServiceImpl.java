package com.alibaba.cloudy.service.impl;


import com.alibaba.cloudy.entities.Results;
import com.alibaba.cloudy.service.ProductHystrixService;
import org.springframework.stereotype.Component;

@Component
public class ProductHystrixServiceImpl implements ProductHystrixService {
    @Override
    public Results<String> override() {
        //
        return new Results<>(500, "Service is fallback", "FAIL");
    }
}
