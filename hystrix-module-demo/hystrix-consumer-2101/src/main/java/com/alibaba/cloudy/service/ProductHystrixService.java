package com.alibaba.cloudy.service;

import com.alibaba.cloudy.entities.Results;
import com.alibaba.cloudy.service.impl.ProductHystrixServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

@Component
@FeignClient(value = "HYSTRIX-PROVIDER-SERVICE2", fallback = ProductHystrixServiceImpl.class)
// Use the FeignClient implementation class to do service fallback
public interface ProductHystrixService {

    @GetMapping(value = "/product/override")
    public Results<String> override();

}
