package com.alibaba.cloudy.controller;

import cn.hutool.core.thread.ThreadUtil;
import com.alibaba.cloudy.entities.Results;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(value = "/product")
@RestController
@Slf4j
public class ProviderController {

    @Value("${server.port}")
    private String serverPort;

    @GetMapping(value = "/normal")
    public Results<String> normal() {
        return new Results<>(200, "Provider is running", String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @HystrixCommand(fallbackMethod = "fallBackForTimeout", commandProperties = {
            @HystrixProperty(name = "execution.timeout.enabled", value = "true"),
            // Service fallback is triggered when this response time is exceeded
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "3000")
    })
    @GetMapping(value = "/timeout/{seconds}")
    public Results<String> timeout(@PathVariable("seconds") Long seconds) {
        ThreadUtil.sleep(seconds * 1000);
        return new Results<>(200, "Provider is running", String.format("SUCCESS(PORT=%s)", serverPort));
    }

    public Results<String> fallBackForTimeout(@PathVariable("seconds") Long seconds) {
        // If a method for service fallback is also declared on the consumer side, service fallback here is masked
        log.info("Provider is timeout");
        return new Results<>(500, "Provider is fallback", String.format("FAIL(PORT=%s)", serverPort));
    }

    @GetMapping(value = "/override")
    public Results<String> override() {
        ThreadUtil.sleep(Integer.MAX_VALUE);
        return new Results<>(200, "Provider is running", String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @HystrixCommand(fallbackMethod = "breakForError", commandProperties = {
            @HystrixProperty(name = "circuitBreaker.enabled", value = "true"),// 是否启用断路器
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "10000"), // 统计的时间范围，默认10s
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "5"),// 时间范围内的访问次数，默认20次
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "60"),// 时间范围内的故障率，默认50%
    })
    @GetMapping(value = "/error/{flag}")
    public Results<String> error(@PathVariable("flag") Integer flag) {
        if (flag > 0) {
            int a = 1 / 0;
        }
        return new Results<>(200, "Provider is running", String.format("SUCCESS(PORT=%s)", serverPort));
    }

    public Results<String> breakForError(@PathVariable("flag") Integer flag) {
        return new Results<>(500, "Provider is break", String.format("FAIL(PORT=%s)", serverPort));
    }

    @GetMapping(value = "/toomany")
    public Results<String> toomany() {
        ThreadUtil.sleep(1000);
        return new Results<>(200, "Provider is running", String.format("SUCCESS(PORT=%s)", serverPort));
    }
}
