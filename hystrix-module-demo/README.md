# Hystrix



### 1. Hystrix

- 服务雪崩

  分布式系统各服务间存在复杂的互相调用，当其中一个服务不可用，往往会导致其他服务的连锁反应（复杂的级联故障），出现链式或扇形服务瘫痪，形成“服务雪崩”。



  Hystrix是服务“断路器”，具备服务降级、服务熔断、服务限流、以及（接近实时的）服务监控等多种功能。

- 服务降级

  当服务接收请求后发生响应故障（发生程序异常、响应超时、内存耗尽、CPU吃紧等）后，为了避免调用方的长时间等待和不可期的异常抛出，服务“降级”的提供一个预期的、可处理的备选响应（FallBack，兜底响应）；

- 服务熔断

  在一定时间内服务发生较多次数的服务降级会进一步引发服务熔断（Open），熔断服务在一定时间后会尝试限流访问（Half-0pen），当服务可承受正常数量的服务请求后，则恢复完全的可用状态（Close）;

  服务熔断的3个状态：Open、Half-Open、Close;

- 服务限流

- 服务监控

  Hystrix Dashboard支持可视化的服务调度监控；



### 2. 搭建Consul服务端

- 下载

```bash
# https://www.consul.io/downloads
```

- 安装与启动

```bash
# 双击安装
# 开发模式启动（程序文件所在路径下执行）
$ consul agent -dev
```

- 访问

```bash
# http://localhost:8500
# 免密登录
```



### 3. 搭建Consul客户端

##### （1）搭建2个业务实例 Provider

- 建立 `hystrix-provider-2301` 子Module

- 在子pom中追加

```xml
<!-- Hystrix -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
</dependency>

<!-- Consul -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-consul-discovery</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableDiscoveryClient` 和`@EnableCircuitBreaker`注解

```java
@SpringBootApplication
@EnableDiscoveryClient
@EnableCircuitBreaker
public class HystrixProvider2301 {
    public static void main(String[] args) {
        SpringApplication.run(HystrixProvider2301.class, args);
    }
}
```

- 在application.yml追加配置

```yml
server:
  port: 2301

spring:
  application:
    name: hystrix-provider-service
  cloud:
    consul:
      host: localhost
      port: 8500
      discovery:
        service-name: ${spring.application.name}
```

- 同样方法创建 `hystrix-provider-2302`，并指定应用名称为 `hystrix-provider-service2`



##### （2）搭建1个业务实例 Consumer

- 建立 `hystrix-consumer-2101` 子Module

- 在子pom中追加

```xml
<!-- Hystrix -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
</dependency>

<!-- Feign -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-openfeign</artifactId>
</dependency>

<!-- Consul -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-consul-discovery</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableDiscoveryClient`、`@EnableFeignClients`和`@EnableHystrix`注解

```java
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@EnableHystrix
public class HystrixConsumer2101 {
    public static void main(String[] args) {
        SpringApplication.run(HystrixConsumer2101.class, args);
    }
}
```

- 在application.yml追加配置

```yml
server:
  port: 2101

spring:
  application:
    name: hystrix-consumer-service
  cloud:
    consul:
      host: localhost
      port: 8500
      discovery:
        service-name: ${spring.application.name}

ribbon:
  ConnectTimeout: 2000 # 建立连接所支持的最大耗时，单位毫秒
  ReadTimeout: 25000 # 读取响应内容所支持的最大耗时

feign:
  hystrix:
    enabled: true

management:
  endpoints:
    web:
      exposure:
        include: "*" # FOR Hystrix Dashboard
```



### 4. 逐一启动各微服务

```bash
# 逐一启动 consul-server-8500.bat（注册中心）、hystrix-provider-2301（微服务实例 生产者）、hystrix-provider-2302（微服务实例 生产者）、hystrix-consumer-2101（微服务实例 消费者）
```



### 5. 服务降级

##### 分类一

- （1）按方法分别指定

```java
// ProviderController.java

@HystrixCommand(fallbackMethod = "fallBackForTimeout", commandProperties = {
  @HystrixProperty(name = "execution.timeout.enabled", value = "true"),
 // Service fallback is triggered when this response time is exceeded
  @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "3000")
})
@GetMapping(value = "/product/timeout/{seconds}")
public Results<String> timeout(@PathVariable("seconds") Long seconds) {
    ThreadUtil.sleep(seconds * 1000);
    return new Results<>(200, "Provider is running", String.format("SUCCESS(PORT=%s)", serverPort));
}

public Results<String> fallBackForTimeout(@PathVariable("seconds") Long seconds) {
// If a method for service fallback is also declared on the consumer side, service fallback here is masked
    log.info("Provider is timeout");
    return new Results<>(500, "Provider is fallback", String.format("FAIL(PORT=%s)", serverPort));
}
```

```java
// Consumer side

@Component
@FeignClient(value = "HYSTRIX-PROVIDER-SERVICE")
public interface ProductFeignService {
    @GetMapping(value = "/product/timeout/{seconds}")
    public Results<String> timeout(@PathVariable("seconds") Long seconds);
}
```

```java
// ConsumerController.java

@Resource
private ProductFeignService productFeignService;
    
@GetMapping("/consumer/product/timeout/{seconds}")
public Results timeout(@PathVariable("seconds") Long seconds) {
    return productFeignService.timeout(seconds);
}
```

```bash
# 测试
$ curl http://localhost:2101/consumer/product/timeout/5
> {"code": 500, "data": "Provider is fallback", "message": "FAIL(PORT=2301)"}
```


- （2）类内声明统一的默认方法

```java
// ConsumerController.java

@RestController
@DefaultProperties(defaultFallback = "defaultFallback")
// Declare a default fallback method inside the class
public class ConsumerController {
    @GetMapping("/consumer/product/default")
    @HystrixCommand
    public Results defaults() {
        int a = 1 / 0;
        return new Results(200, "Consumer is running", "SUCCESS");
    }

    public Results defaultFallback() {
        return new Results(500, "Default fallback", "FAIL");
    }
}
```

```bash
$ curl http://localhost:2101/consumer/product/default
> {"code": 500, "data": "Default fallback", "message": "FAIL"}
```

- （3）FeignClient的实现类

```java
// ConsumerController.java

@GetMapping("/consumer/product/override")
public Results override() {
    return productHystrixService.override();
}
```
```java
// Consumer side

@Component
@FeignClient(value = "HYSTRIX-PROVIDER-SERVICE2", fallback = ProductHystrixServiceImpl.class)
// Use the FeignClient implementation class to do service fallback
public interface ProductHystrixService {
    @GetMapping(value = "/product/override")
    public Results<String> override();
}

@Component
public class ProductHystrixServiceImpl implements ProductHystrixService {
    @Override
    public Results<String> override() {
        return new Results<>(500, "Service is fallback", "FAIL");
    }
}
```

```bash
$ curl http://localhost:2101/consumer/product/default
> {"code": 500, "data": "Service is fallback", "message": "FAIL"}
```



##### 分类二

- （1）服务端的服务降级

```
# 参考 分类一 按方法分别指定
```

- （2）消费端的服务降级

```java
// ConsumerController.java

@GetMapping("/consumer/product/timeout/{seconds}")
@HystrixCommand(fallbackMethod = "fallBackForTimeout", commandProperties = {
@HystrixProperty(name = "execution.timeout.enabled", value = "true"),
// Service fallback is triggered when this response time is exceeded
@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "18000")
})
public Results timeout(@PathVariable("seconds") Long seconds) {
    return productFeignService.timeout(seconds);
}

public Results fallBackForTimeout(@PathVariable("seconds") Long seconds) {
    return new Results(500, "Consumer is fallback", "FAIL");
}
```

```bash
$ curl http://localhost:2101/consumer/product/timeout/1
> {"code": 500, "data": "Consumer is fallback", "message": "FAIL"}
```



### 6. 服务熔断

```java
// ProviderController.java

@HystrixCommand(fallbackMethod = "breakForError", 
commandProperties = {
    @HystrixProperty(name = "circuitBreaker.enabled", value = "true"),// 是否启用断路器
    @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "10000"), // 统计的时间范围，默认10s
    @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "5"),// 时间范围内的访问次数，默认20次
    @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "60"),// 时间范围内的故障率，默认50%
})
@GetMapping(value = "/product/error/{flag}")
public Results<String> error(@PathVariable("flag") Integer flag) {
    if (flag > 0) {
      int a = 1 / 0;
    }
    return new Results<>(200, "Provider is running", String.format("SUCCESS(PORT=%s)", serverPort));
}

public Results<String> breakForError(@PathVariable("flag") Integer flag) {
    return new Results<>(500, "Provider is break", String.format("FAIL(PORT=%s)", serverPort));
}
```

```java
// Consumer side

@Component
@FeignClient(value = "HYSTRIX-PROVIDER-SERVICE")
public interface ProductFeignService {
    @GetMapping(value = "/product/error/{flag}")
    public Results<String> error(@PathVariable("flag") Integer flag);
}
```

```java
// ConsumerController.java

@Resource
private ProductFeignService productFeignService;

/**
  * 服务熔断：10s内5次访问3次失败就可以引发服务熔断（发生熔断后的5s自动恢复）
  */
@GetMapping(value = "/consumer/product/error/{flag}")
public Results<String> error(@PathVariable("flag") Integer flag) {
    return productFeignService.error(flag);
}
```

```bash
$ curl http://localhost:2101/consumer/product/error/0
> {"code": 200, "data": "Provider is running", "message": "SUCCESS(PORT=2301)"}
# 非熔断（Close）

$ curl http://localhost:2101/consumer/product/error/1
> {"code": 500, "data": "Provider is break", "message": "FAIL(PORT=2301)"}

$ curl http://localhost:2101/consumer/product/error/1
$ curl http://localhost:2101/consumer/product/error/1
$ curl http://localhost:2101/consumer/product/error/1
$ curl http://localhost:2101/consumer/product/error/1
# 此时发生熔断（Open）

$ curl http://localhost:2101/consumer/product/error/0
> {"code": 500, "data": "Provider is break", "message": "FAIL(PORT=2301)"}
# 熔断条件下，即使发/error/0也会执行服务降级

# 注意！！！熔断仅针对 /consumer/product/error
# 其他服务请求，如/consumer/product/normal是可以正常响应的

# 过5秒（Half-Open）
# 完全恢复（Close）
$ curl http://localhost:2101/consumer/product/error/0
> {"code": 200, "data": "Provider is running", "message": "SUCCESS(PORT=2301)"}
```



### 7. 服务限流

```
# TODO
```



### 8. 服务监控 Hystrix Dashboard

- 建立 `hystrix-dashboard-2000` 子Module

- 在子pom中追加

```xml
<!-- Dashboard -->
<dependency>
    <groupId>com.netflix.hystrix</groupId>
    <artifactId>hystrix-javanica</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-netflix-hystrix-dashboard</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableHystrixDashboard` 注解

```java
@SpringBootApplication
@EnableHystrixDashboard
public class HystrixDashboard2000 {
    public static void main(String[] args) {
        SpringApplication.run(HystrixDashboard2000.class, args);
    }
}
```

- 在application.yml追加配置

```yml
server:
  port: 2000

hystrix:
  dashboard:
    proxy-stream-allow-list: "localhost"
```

- 在要监控的服务启动类上添加

```java
// HystrixProvider2301.java

@Bean
public ServletRegistrationBean getServlet() {
    HystrixMetricsStreamServlet streamServlet = new HystrixMetricsStreamServlet();
    ServletRegistrationBean registrationBean = new ServletRegistrationBean(streamServlet);
    registrationBean.setLoadOnStartup(1);
    registrationBean.addUrlMappings("/hystrix.stream");
    registrationBean.setName("HystrixMetricsStreamServlet");
    return registrationBean;
}
```

- 在要监控服务的application.yml追加配置

```yml
management:
  endpoints:
    web:
      exposure:
        include: "*"
```

```bash
# 逐一启动 consul-server-8500.bat（注册中心）、hystrix-provider-2301（微服务实例 生产者）、hystrix-consumer-2101（微服务实例 消费者）、hystrix-dashboard-2000（监控中心）

# 访问HystrixDashboard主页 http://localhost:2000/hystrix
# 配置监控地址为 http://localhost:2301/actuator/hystrix.stream 或 http://localhost:2301/hystrix.stream 后点击“Monitor Stream”
# hystrix-provider-2301 刚启动，从未被调用过，在HystrixDashboard主页上的 Circuit和Thread Pools模块均只显示“Loading...”

# 发起一个有服务降级的接口请求（方法上需有@HystrixCommand注解），如
$ curl http://localhost:2101/consumer/product/error/1
# 刷新HystrixDashboard主页，查看监控状态曲线
```