package com.alibaba.cloudy;


import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

@SpringBootApplication
@Slf4j
@EnableHystrixDashboard
public class HystrixDashboard2000 {

    public static void main(String[] args) {
        SpringApplication.run(HystrixDashboard2000.class, args);
        log.info("Ψ(￣∀￣)ΨΨ(￣∀￣)ΨΨ(￣∀￣)Ψ(づ￣ 3￣)づ(づ￣ 3￣)づ(づ￣ 3￣)づ(づ￣ 3￣)づ");
    }

}
