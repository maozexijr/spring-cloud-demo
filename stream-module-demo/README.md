# Spring Cloud Stream



### 1. Spring Cloud Stream

  Spring Cloud Stream屏蔽消息中间件底层API，自动适配多种类MQ（RabbitMQ、Kafka），做到一次编码随机应变的目的。

  Spring Cloud Stream的业务流程：

```bash
# Provider  → | → Source → Channel(output) → Binder ↘
#             |                                    MQ(Rabbit/Kafka)
#             |                                    ↙   /
# Consumer1 ← | ← Sink ← Channel(input) ← Binder      /
# Consumer2 ← | ← Sink ← Channel(input) ← Binder    ↙
```



### 2. 安装消息中间件

  Spring Cloud Bus的使用是基于消息中间件的，目前仅支持RabbitMQ和Kafka（不支持RocketMQ和ActiveMQ）

**环境准备**

- Erlang安装与配置

（1）下载并以默认方式安装

```bash
https://erlang.org/download/otp_win64_24.0.exe
```

（2）配置环境变量

```bash
ERLANG_HOME=C:\Program Files\erl-24.0
Path=%ERLANG_HOME%\bin;
```

（3）校验是否安装成功

```bash
$ erl
> Eshell V12.0  (abort with ^G)
```


- RabbitMQ安装与配置

（1）下载并以默认方式安装

```bash
# https://www.rabbitmq.com/install-windows.html
# 注意安装路径和主机名不要有中文
```

（2）安装相关插件

```bash
# 在安装目录的sbin下以管理员权限执行
# 菜单栏找到 “RabbitMQ Command Prompt(sbin dir)”
$ rabbitmq-plugins enable rabbitmq_management
```

（3）启动服务

```bash
# 在安装目录的sbin下以管理员权限执行 rabbitmq-server.bat
```

（4）访问主页

```bash
# 访问 http://localhost:15672/
# 默认账密guest/guest
```



### 3. 搭建Consul服务端

- 下载

```bash
# https://www.consul.io/downloads
```

- 安装与启动

```bash
# 双击安装
# 开发模式启动（程序文件所在路径下执行）
$ consul agent -dev
```

- 访问

```html
# http://localhost:8500
# 免密登录
```



### 4. 搭建Consul客户端

##### （1）搭建1个业务实例 Provider

- 建立 `stream-provider-2301` 子Module

- 在子pom中追加

```xml
<!-- Stream -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-stream-rabbit</artifactId>
</dependency>

<!-- Consul -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-consul-discovery</artifactId>
</dependency>
```

- 编写SpringBoot启动类

```java
@SpringBootApplication
@EnableDiscoveryClient
public class StreamProvider2301 {
    public static void main(String[] args) {
        SpringApplication.run(StreamProvider2301.class, args);
    }
}
```

- 在application.yml追加配置

```yml
server:
  port: 2301

spring:
  application:
    name: stream-provider-service
  cloud:
    consul:
      host: localhost
      port: 8500
      discovery:
        service-name: ${spring.application.name}
    stream:
      binders:
        defaultRabbit: # 声明一个绑定器
          type: rabbit
          environment:
            spring:
              rabbitmq:
                host: localhost
                port: 5672
                username: guest
                password: guest
      bindings:
        output: # org.springframework.cloud.stream.messaging.Source中声明了一个名为output的MessageChannel
          destination: stream-module-exchange # 队列名，Rabbit中的Exchange，Kafka中的Topic
          content-type: application/json # 或 text/plain
          binder: defaultRabbit # 使用上面声明的绑定器

# Provider  → | → Source → Channel(output) → Binder ↘
#             |                                    MQ(Rabbit/Kafka)
#             |                                    ↙   /
# Consumer1 ← | ← Sink ← Channel(input) ← Binder      /
# Consumer2 ← | ← Sink ← Channel(input) ← Binder    ↙
```

- 在Service实现追加`@EnableBinding(Source.class)`注解

```java
public interface MessageService<T> {
    public void send(T info);
}


@EnableBinding(Source.class) // !important
@Slf4j
public class MessageServiceImpl<T> implements MessageService<T> {

// Provider  → | → Source → Channel(output) → Binder ↘
//             |                                    MQ(Rabbit/Kafka)
//             |                                    ↙   /
// Consumer1 ← | ← Sink ← Channel(input) ← Binder      /
// Consumer2 ← | ← Sink ← Channel(input) ← Binder    ↙

    @Value("${server.port}")
    private String serverPort;

    @Resource
    private MessageChannel output;

    @Override
    public void send(T info) {
        output.send(MessageBuilder.withPayload(info).build());
        log.info("@{} POST\t\"{}\"", serverPort, info);
    }

}
```

- 编写Controller层

```java
@RestController
@Slf4j
public class ProviderController {

    @Value("${server.port}")
    private String serverPort;

    @Resource
    private MessageService messageService;

    @GetMapping(value = "/message/send/{info}")
    public Results getOne(@PathVariable("info") String info) {
        if (StrUtil.isBlank(info)) {
            info = RandomUtil.randomString(10);
        }
        messageService.send(info);
        return new Results<>(200, null, String.format("SUCCESS(PORT=%s)", serverPort));
    }

}
```


##### （2）搭建2个业务实例 Consumer

- 建立 `stream-consumer-2101` 子Module

- 在子pom中追加

```xml
<!-- Stream -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-stream-rabbit</artifactId>
</dependency>

<!-- Consul -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-consul-discovery</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableDiscoveryClient` 注解

```java
@SpringBootApplication
@Slf4j
@EnableDiscoveryClient
public class StreamConsumer2101 {
    public static void main(String[] args) {
        SpringApplication.run(StreamConsumer2101.class, args);
    }
}
```

- 在application.yml追加配置

```yml
server:
  port: 2101

spring:
  application:
    name: stream-consumer-service
  cloud:
    consul:
      host: localhost
      port: 8500
      discovery:
        service-name: ${spring.application.name}
    stream:
      binders:
        defaultRabbit: # 声明一个绑定器
          type: rabbit
          environment:
            spring:
              rabbitmq:
                host: localhost
                port: 5672
                username: guest
                password: guest
      bindings:
        input: # org.springframework.cloud.stream.messaging.Sink中声明了一个名为input的MessageChannel
          destination: stream-module-exchange # 队列名，Rabbit中的Exchange，Kafka中的Topic
          content-type: application/json # 或 text/plain
          binder: defaultRabbit # 使用上面声明的绑定器
          group: stream-module-group # !important
          # 分组：（1）同组竞争，避免重复消费 （2）消息持久化，重启消费者时可以读取历史的未处理消息


# Provider  → | → Source → Channel(output) → Binder ↘
#             |                                    MQ(Rabbit/Kafka)
#             |                                    ↙   /
# Consumer1 ← | ← Sink ← Channel(input) ← Binder      /
# Consumer2 ← | ← Sink ← Channel(input) ← Binder    ↙
```

- 在消息处理类里追加`@EnableBinding(Sink.class)`注解

```java
@EnableBinding(Sink.class) // !important
@Component
@Slf4j
public class ReceiveMessage {

// Provider  → | → Source → Channel(output) → Binder ↘
//             |                                    MQ(Rabbit/Kafka)
//             |                                    ↙   /
// Consumer1 ← | ← Sink ← Channel(input) ← Binder      /
// Consumer2 ← | ← Sink ← Channel(input) ← Binder    ↙

    @Value("${server.port}")
    private String serverPort;

    @StreamListener(Sink.INPUT) // !important
    public void input(Message<Object> message) {
        log.info("@{} GET\t\"{}\"", serverPort, message.getPayload());
    }

}
```

- 同样方法创建 `stream-consumer-2102`



### 4. 逐一启动各微服务

```bash
# 逐一启动 rabbitmq-server.bat（消息中间件）、consul-server-8500.bat（注册中心）、stream-provider-2301（微服务实例 生产者）、stream-consumer-2101（微服务实例 消费者）、stream-consumer-2102（微服务实例 消费者）
```



### 5. 测试用例

```bash
# 推送消息（建议使用浏览器）
$ curl http://localhost:2301/message/send/哈哈哈
> {"code":200,"data":null,"message":"SUCCESS(PORT=2301)"}
$ curl http://localhost:2301/message/send/嘿嘿嘿
> {"code":200,"data":null,"message":"SUCCESS(PORT=2301)"}

# 查看 stream-provider-2301 控制台日志
# @2301 POST	"哈哈哈"
# @2301 POST	"嘿嘿嘿"

# 查看 stream-consumer-2101 控制台日志
# @2101 GET	"嘿嘿嘿"

# 查看 stream-consumer-2102 控制台日志
# @2102 GET	"哈哈哈"
```

