package com.alibaba.cloudy.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

@EnableBinding(Sink.class) // !important
@Component
@Slf4j
public class ReceiveMessage {

// Provider  → | → Source → Channel(output) → Binder ↘
//             |                                    MQ(Rabbit/Kafka)
//             |                                    ↙   /
// Consumer1 ← | ← Sink ← Channel(input) ← Binder      /
// Consumer2 ← | ← Sink ← Channel(input) ← Binder    ↙

    @Value("${server.port}")
    private String serverPort;

    @StreamListener(Sink.INPUT) // !important
    public void input(Message<Object> message) {
        log.info("@{} GET\t\"{}\"", serverPort, message.getPayload());
    }

}
