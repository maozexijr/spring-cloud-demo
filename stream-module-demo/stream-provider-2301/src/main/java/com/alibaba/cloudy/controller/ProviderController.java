package com.alibaba.cloudy.controller;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.cloudy.entities.Results;
import com.alibaba.cloudy.service.MessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@Slf4j
public class ProviderController {

    @Value("${server.port}")
    private String serverPort;

    @Resource
    private MessageService messageService;

    @GetMapping(value = "/message/send/{info}")
    public Results getOne(@PathVariable("info") String info) {
        if (StrUtil.isBlank(info)) {
            info = RandomUtil.randomString(10);
        }
        messageService.send(info);
        return new Results<>(200, null, String.format("SUCCESS(PORT=%s)", serverPort));
    }

}
