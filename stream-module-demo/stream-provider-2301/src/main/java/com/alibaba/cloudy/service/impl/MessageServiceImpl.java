package com.alibaba.cloudy.service.impl;

import com.alibaba.cloudy.service.MessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.MessageChannel;

import javax.annotation.Resource;

@EnableBinding(Source.class) // !important
@Slf4j
public class MessageServiceImpl<T> implements MessageService<T> {

// Provider  → | → Source → Channel(output) → Binder ↘
//             |                                    MQ(Rabbit/Kafka)
//             |                                    ↙   /
// Consumer1 ← | ← Sink ← Channel(input) ← Binder      /
// Consumer2 ← | ← Sink ← Channel(input) ← Binder    ↙

    @Value("${server.port}")
    private String serverPort;

    @Resource
    private MessageChannel output;

    @Override
    public void send(T info) {
        output.send(MessageBuilder.withPayload(info).build());
        log.info("@{} POST\t\"{}\"", serverPort, info);
    }

}
