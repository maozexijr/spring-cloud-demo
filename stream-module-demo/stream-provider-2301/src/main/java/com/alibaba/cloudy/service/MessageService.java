package com.alibaba.cloudy.service;

public interface MessageService<T> {

    public void send(T info);

}
