package com.alibaba.cloudy;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@Slf4j
@EnableDiscoveryClient
public class NacosProvider3301 {

    public static void main(String[] args) {
        SpringApplication.run(NacosProvider3301.class, args);
        log.info("(づ￣ 3￣)づ(づ￣ 3￣)づ(づ￣ 3￣)づ(づ￣ 3￣)づΨ(￣∀￣)ΨΨ(￣∀￣)ΨΨ(￣∀￣)Ψ");
    }

}
