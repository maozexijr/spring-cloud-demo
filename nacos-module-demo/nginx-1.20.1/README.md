# Nginx



### 1. Nginx

```
高性能的HTTP、反向代理服务器，占用内存小，并发能力强；

（1）反向代理

```text
正向代理（如VPN）：
  Client（要访问192.168.1.1） ➝ Forward proxy（我是127.0.0.1） ➝ Target（我是192.168.1.1）

反向代理（如Nginx）：
  Client（要访问192.168.1.1） ➝ Reverse proxy（我是192.168.1.1） ➝ Target（127.0.0.1）
                                                              ↘ Target（127.0.0.2）
                                                             ↘ Target（127.0.0.3）
```

正向代理需要客户端配置地址映射，由代理服务器间接访问目标服务器；

反向代理无需客户端额外配置，无感的“直连”目标服务器，由反向代理服务器决定具体由哪个目标服务器提供服务；

正向代理和反向代理的核心区别：目标服务器（响应服务器）的选择是由客户端决定还是代理服务器决定；

（2）负载均衡

反向代理服务器平均的分配来自客户端的请求到各响应服务器上，实现各响应服务器的负载均衡；

（3）动静分离

将“动态”的程序资源和“静态”的文件资源分别部署到不同服务器，由反向代理服务器区别访问；

```



### 2. 下载与安装

```bash
# 安装依赖
$ yum -y install make zlib zlib-devel gcc-c++ libtool  openssl openssl-devel pcre

# 下载安装
$ cd /usr/local/src
$ wget https://nginx.org/download/nginx-1.20.1.tar.gz
$ tar -zxvf ./nginx-1.20.1.tar.gz
$ cd ./nginx-1.20.1
$ ./configure
$ make && make install

# 校验可用
$ cd /usr/local/nginx/sbin
$ ./nginx -v
```



### 3.常用命令

```bash
# 查看版本号
$ ./nginx -v
# 启动
$ ./nginx
# 启动
$ ./nginx -s stop
# 重新载入配置
$ ./nginx -s reload

# 查看进程
$ ps -ef | grep nginx
# 查询本机IP
$ ifconfig # 或 hostname -i
           # Windows下ipconfig
# 访问主页
$ curl http://192.168.209.129/
```



### 4. 配置文件

```bash
# /usr/local/nginx/conf/nginx.conf

# ┏━ gloab ━ worker_processes # 最大并发数
# ╋━ events ━ worker_connections # 最大连接数
# ┃       ┏━ gloab
# ┗━ http ┫                  ┏━ listen # 监听端口
#         ┃         ┏━ gloab ┫
#         ┗━ server ┫        ┗━ server_name # 监听域名
#                   ┗━ location ━ proxy_pass # 转发路径
```

```bash
# location 参数
# （1）location = 严格匹配
# （2）location ~ 正则匹配，区分大小写
# （3）location ~* 正则匹配，不区分大小写
# （4）location ^~ 不匹配正则
```



### 5. 请求转发（反向代理）

- 配置

```json
# /usr/local/nginx/conf/nginx.conf
server {
  listen       8090;
  server_name  localhost;

  location / {
    proxy_pass http://localhost:8091/;
    # 由 localhost:8090 转发到 localhost:8091
  }
  
  # location ~ 表示启用正则表达式
  location ~ /express/ {
    proxy_pass http://localhost:8092/;
    # 由 localhost:8090/express/ 转发到 localhost:8092/express/
  }
}
```

- 测试

```bash
$ curl http://localhost:8090
$ curl http://localhost:8090/express/
```



### 6. 负载均衡

- 配置

```json
# /usr/local/nginx/conf/nginx.conf
upstream demo { # 自定义服务名
  server 127.0.0.1:8091;
  server 127.0.0.1:8092;
  server 127.0.0.1:8093;
}

server {
  listen       8090;
  server_name  localhost;

  location / {
    proxy_pass http://demo;
    # 由 localhost:8090 转发到 127.0.0.1:809x
  }
}
```

- 测试

```bash
$ curl http://localhost:8090/demo
$ curl http://localhost:8090/demo
```



### 7. 动静分离（静态文件）

- 配置

```json
# /usr/local/nginx/conf/nginx.conf
server {
  listen       8090;
  server_name  localhost;

  location ~ /nginx/ {
    alisa /usr/local/nginx/; # 别名替换 alisa
    enableindex on;
    # 访问localhost:8090/nginx/，展示/usr/local/nginx/下所有文件
  }

  location ~ /conf/ {
    root /usr/local/nginx/; # 路径下匹配 root
    enableindex on;
    # 访问localhost:8090/conf/，展示/usr/local/nginx/conf/下所有文件
  }
}
```

- 测试

```bash
$ curl http://localhost:8090/nginx/
$ curl http://localhost:8090/conf/
```



### 8. Nginx集群（高可用、主备机）

- 服务结构

```
# 每台服务器上都要部署一套 keepalived、nginx
#                     ┏━━━━━━━━━━━━━━━┓
#                     ┃   nginx 0    ┃
#                     ┃      ⇧       ┃
# ┏━━━━━━━━━━━━━━━┓     ┃ keepalived 0 ┃
# ┃   Browser    ┃ ⇨  ┗━━━━━━━━━━━━━━━┛
# ┗━━━━━━━━━━━━━━━┛     ┏━━━━━━━━━━━━━━━┓
#                     ┃ keepalived 1 ┃
#                     ┃      ⇩       ┃
#                     ┃   nginx 1    ┃
#                     ┗━━━━━━━━━━━━━━━┛
```


- keepalived

```bash
# 安装
$ yum install -y keepalived
# 校验
$ rpm -qa keepalived

# 启动
$ systemctl start keepalived.service
# 校验
$ ps -ef | grep keepalived
```

- 配置文件

```bash
# /etc/keepalived/keepalived.conf

global_defs {
   notification_email {
     acassen@firewall.loc
     failover@firewall.loc
     sysadmin@firewall.loc
   }
   notification_email_from Alexandre.Cassen@firewall.loc
   smtp_server 192.168.200.1
   smtp_connect_timeout 30
   router_id LVS_DEVEL # 路由主键，应追加配置 echo "127.0.0.1 LVS_DEVEL" >> /etc/hosts 并 ipconfig /flushdns
   vrrp_skip_check_adv_addr
   vrrp_strict
   vrrp_garp_interval 0
   vrrp_gna_interval 0
}

vrrp_script chk_http_port {
    script "/etc/keepalived/nginx_check.sh" # 脚本的路径
    interval 2 # 脚本执行的时间间隔，秒
    weight 2 # 权重
}

vrrp_instance VI_1 {
    state MASTER # 区别主备，MASTER或BACKUP
    interface eth0 # 网卡名称，用ifconfig查看当前主机上的网卡
    virtual_router_id 51 # 虚拟路由主键，主备机上此值必须相同
    priority 100 # 优先级，主机值大，备用机小
    advert_int 1 # 心跳包的时间间隔
    authentication {
        auth_type PASS # 权限校验方式，密码
        auth_pass 1111 # 密码为 1111
    }
    virtual_ipaddress {
        192.168.200.16 # 虚拟IP，即Nginx服务的代理地址
    }
}
```

- 脚本

```bash
# /etc/keepalived/nginx_check.sh

#!/bin/bash
A=`ps -C nginx ¨Cno-header |wc -l`
if [ $A -eq 0 ];then
    /usr/local/nginx/sbin/nginx
    sleep 2
    if [ `ps -C nginx --no-header |wc -l` -eq 0 ];then
        killall keepalived
    fi
fi
```

- 测试

```bash
$ curl http://192.168.200.16:8090/nginx/ # 配置的虚拟IP
```