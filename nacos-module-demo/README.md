# Alibaba Nacos



### 1. Nacos

  官网：https://nacos.io/zh-cn/docs/what-is-nacos.html

- Nacos全称Naming Configuration Services（服务注册+配置管理），其在功能上等价于 Eureka + Config + Bus，集服务注册、发现、配置和消息总线于一身。

- Nacos中的几个概念：

（1）命名空间（**Namespace**）包含多个分组，用以隔离环境类型，开发（DEV）、测试（TEST）、生产（PROD）等，默认 `public` ；

（2）分组（**Group**）包含多个微服务，用以减少跨域访问、尽量同组调用，如为了容灾和提高响应速度，将服务器分别部署到北京、上海和广州，各自归为一组，默认 `DEFAULT_GROUP` ，一个分组包含多个微服务；

（3）微服务（**Service**）包含多个集群，在分布式系统中一个完整的业务流程往往被拆分成多个模块，模块间互为调用，从而完成整体业务逻辑；

（4）集群（**Cluster**）包含多个微服务实例，同一集群下的各微服务实例往往处理同一业务模块（同一套代码），默认 `DEFAULT` ；

（5）微服务实例（**Instance**），业务模块的具体处理单元


- 各类“注册中心”的对比

|                  | Nacos | Eureka | Consul | Zookeeper |
| -----------------| ----- | ------ | ------ | --------- |
| CAP模型           | CP/AP | AP     | CP     | CP        |
| 健康检查           | TCP/HTTP/MySQL/Client Beat | Client Beat | TCP/HTTP/gRPC/Cmd | Client Beat |
| 访问协议           | HTTP/DNS/UDP | HTTP | HTTP/DNS | TCP |
| 均衡负载           | Weight/DSL/MetaData/CMDB | Ribbon | Fabio | × |
| 服务熔断（雪崩保护） | √    | √       | ×      | ×         |
| 服务注销           | √    | √       | ×      | √         |
| 服务监听           | √    | √       | √      | √         |
| 多数据中心         | √    | √       | √       | ×         |
| 跨注册中心（集群）   | √    | ×       | √      | ×         |
| 集成到SpringCloud  | √    | √       | √      | ×         |
| 集成到Dubbo        | √    | ×       | ×      | √         |
| 集成到K8s          | √    | ×       | √      | ×         |



### 2. 搭建Nacos服务端（单机版）

- 下载

```bash
# https://github.com/alibaba/nacos/tags
```

- 启动

```bash
# 程序文件所在bin路径下执行
$ startup.cmd -m standalone
$ ./startup.sh -m standalone # 或
```

- 访问

```bash
# http://localhost:8848/nacos
# 默认账密 nacos/nacos
```

- 命名空间

  新增 命名空间 `nacos-demo-namespace`

- 配置文件

 在 `配置管理` > `配置列表` > `配置列表` 中新增配置

```yaml
# Data Id: ${spring.application.name}-${spring.profiles.active}.${spring.cloud.nacos.config.file-extension}
# Group: ${spring.cloud.nacos.config.group}

# Data Id: nacos-consumer-service-dev.yaml
# Group: nacos-demo-group
# Namespace: nacos-demo-namespace

profile:
  name: nacos-consumer-service-dev.yaml
  version: v1.0
```

```yaml
# Data Id: ${spring.application.name}-${spring.profiles.active}.${spring.cloud.nacos.config.file-extension}
# Group: ${spring.cloud.nacos.config.group}

# Data Id: nacos-provider-service-dev.yaml
# Group: nacos-demo-group
# Namespace: nacos-demo-namespace

profile:
  name: nacos-provider-service-dev.yaml
  version: v1.0
```



### 3. 搭建Nacos客户端

##### （1）搭建2个业务实例  Provider

- 建立 `nacos-provider-3301` 子Module

- 在子pom中追加

```xml
<!-- Nacos -->
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
</dependency>
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableDiscoveryClient` 注解

```java
@SpringBootApplication
@EnableDiscoveryClient
public class NacosProvider3301 {
    public static void main(String[] args) {
        SpringApplication.run(NacosProvider3301.class, args);
    }
}
```

- 在bootstrap.yml追加配置

```yml
server:
  port: 3301

spring:
  application:
    name: nacos-provider-service
  cloud:
    nacos:
      discovery:
        server-addr: localhost:8848 # 注册中心
        group: nacos-demo-group
        namespace: nacos-demo-namespace
      config:
        server-addr: localhost:8848 # 配置中心
        file-extension: yaml # 配置文件格式，支持 yaml、xml、properties、json、text、html
        group: nacos-demo-group
        namespace: nacos-demo-namespace

management:
  endpoints:
    web:
      exposure:
        include: "*" # 暴露/actuator
```

- 在application.yml追加配置

```yml
# Data Id: ${spring.application.name}-${spring.profiles.active}.${spring.cloud.nacos.config.file-extension}
# Group: ${spring.cloud.nacos.config.group}

# Data Id: nacos-provider-service-dev.yaml
# Group: nacos-demo-group
# Namespace: nacos-demo-namespace

spring:
  profiles:
    active: dev # 开发环境，测试环境test
```

- 编写Controller层

```java
@RestController
@Slf4j
@RefreshScope // ! important
public class ProviderController {

    @Value("${server.port}")
    private String serverPort;

    @Value("${profile.name}")
    private String profileName;

    @Value("${profile.version}")
    private String profileVersion;

    @GetMapping(value = "/product/get/{id}")
    public Results get(@PathVariable("id") Long id) {
        return new Results<>(200, id, String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @GetMapping(value = "/profile/name")
    public Results<String> getName() {
        return new Results<>(200, profileName, String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @GetMapping(value = "/profile/version")
    public Results<String> getVersion() {
        return new Results<>(200, profileVersion, String.format("SUCCESS(PORT=%s)", serverPort));
    }

}
```

- 同样方法创建 `nacos-provider-3302`



##### （2）搭建1个业务实例 Consumer

- 建立 `nacos-consumer-3101` 子Module

- 在子pom中追加

```xml
<!-- Nacos -->
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
</dependency>
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableDiscoveryClient` 注解

```java
@SpringBootApplication
@EnableDiscoveryClient
public class NacosConsumer3101 {
    public static void main(String[] args) {
        SpringApplication.run(NacosConsumer3101.class, args);
    }
}
```

- 在bootstrap.yml追加配置

```yml
server:
  port: 3101

spring:
  application:
    name: nacos-consumer-service
  cloud:
    nacos:
      discovery:
        server-addr: localhost:8848 # 注册中心
        group: nacos-demo-group
        namespace: nacos-demo-namespace
      config:
        server-addr: localhost:8848 # 配置中心
        file-extension: yaml # 配置文件格式，支持 yaml、xml、properties、json、text、html
        group: nacos-demo-group
        namespace: nacos-demo-namespace

profile:
  name: bootstrap.yaml
  version: v1.0

management:
  endpoints:
    web:
      exposure:
        include: "*" # 暴露/actuator
```

- 在application.yml追加配置

```yml
# Data Id: ${spring.application.name}-${spring.profiles.active}.${spring.cloud.nacos.config.file-extension}
# Group: ${spring.cloud.nacos.config.group}

# Data Id: nacos-consumer-service-dev.yaml
# Group: nacos-demo-group
# Namespace: nacos-demo-namespace

spring:
  profiles:
    active: dev # 开发环境，测试环境test
```

- 添加RestTemplate配置

```java
@Configuration
public class ApplicationContextConfig {

    @Bean
    @LoadBalanced // !important
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}
```

- 编写Controller类

```java
@RequestMapping(value = "/consumer")
@RestController
@Slf4j
@RefreshScope // ! important
public class ConsumerController {

    // !important: Service names are case-sensitive
    private static final String PROVIDER_URL = "http://nacos-provider-service";

    @Value("${server.port}")
    private String serverPort;

    @Value("${profile.name}")
    private String profileName;

    @Value("${profile.version}")
    private String profileVersion;

    @Resource
    private RestTemplate restTemplate;

    @GetMapping("/product/get/{id}")
    public Results get(@PathVariable("id") Long id) {
        return restTemplate.getForObject(PROVIDER_URL + "/product/get/" + id, Results.class);
    }

    @GetMapping(value = "/profile/name")
    public Results<String> getName() {
        return new Results<>(200, profileName, String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @GetMapping(value = "/profile/version")
    public Results<String> getVersion() {
        return new Results<>(200, profileVersion, String.format("SUCCESS(PORT=%s)", serverPort));
    }

}
```



### 4. 逐一启动各微服务

```bash
# 逐一启动 nacos-server-8848（注册中心、配置中心）、nacos-provider-3301（微服务实例 生产者）、nacos-provider-3302（微服务实例 生产者）、nacos-consumer-3101（微服务实例 消费者）
```



### 5. 测试用例（单机版）

```bash
# 访问 http://localhost:8848/nacos/#/serviceManagement 查看服务列表
>        服务名               分组名称           集群数目 实例数 健康实例数
> nacos-provider-service nacos-demo-group         1      2       2
> nacos-consumer-service nacos-demo-group         1      1       1

# 测试服务间的通信和负载均衡
$ curl http://localhost:3101/consumer/product/get/1
> {"code":200,"data":1,"message":"SUCCESS(PORT=3301)"}
$ curl http://localhost:3101/consumer/product/get/1
> {"code":200,"data":1,"message":"SUCCESS(PORT=3302)"}

# 测试“配置中心”功能
$ curl http://localhost:3101/consumer/profile/name
> {"code":200,"data":"nacos-consumer-service-dev.yaml","message":"SUCCESS(PORT=3101)"}
$ curl http://localhost:3101/consumer/profile/version
> {"code":200,"data":"v1.0","message":"SUCCESS(PORT=3101)"}

$ curl http://localhost:3301/profile/name
> {"code":200,"data":"nacos-provider-service-dev.yaml","message":"SUCCESS(PORT=3301)"}
$ curl http://localhost:3301/profile/version
> {"code":200,"data":"v1.0","message":"SUCCESS(PORT=3301)"}

# http://localhost:8848/nacos/#/configurationManagement?namespace=nacos-demo-namespace
# 修改 nacos-consumer-service-dev.yaml 或 nacos-provider-service-dev.yaml 中的配置参数 `version`
# 再次 curl http://localhost:3301/profile/version
```



### 6. 搭建Nacos服务端（集群版）

- 创建名为 `nacos_config` 的数据库

- 使用脚本 `nacos-mysql.sql` 在 `nacos_config` 库内建表

```bash
# nacos/conf/nacos-mysql.sql
```

- 修改 application.properties

```yml
# nacos/conf/application.properties
server.port=8848 # 注意

spring.datasource.platform=mysql
  
db.num=1
db.url.0=jdbc:mysql://127.0.0.1:3306/nacos_config?characterEncoding=utf8&useSSL=false&serverTimezone=Asia/Shanghai&allowPublicKeyRetrieval=true&connectTimeout=3000&socketTimeout=5000&autoReconnect=true
db.user=root
db.password=123456
# 参考 https://nacos.io/zh-cn/docs/deployment.html
```

- 新建 `cluster.conf`

```yml
# nacos/conf/cluster.conf

127.0.0.1:8844
127.0.0.1:8846
127.0.0.1:8848
# 127.0.0.1仅作示例，使用时以实际IP为准
# Nacos 2.x版增加了gRPC方式的健康检查，gRPC所需的两个端口由${server.port}偏移计算得来，故同主机下的集群nacos端口不可以+1连续 （参考 https://nacos.io/zh-cn/docs/2.0.0-compatibility.html）
```

- 重命名程序文件 `nacos` 为 `nacos-8848`

- 同样方法创建 `nacos-8846` 和 `nacos-8844`

- 修改 `bootstrap.yml`

```yml
# 分别修改 nacos-provider-3301、nacos-provider-3302、nacos-consumer-3101的 bootstrap.yml
spring:
  cloud:
    nacos:
      discovery:
        server-addr: localhost:8800/cluster/ # 注册中心
        group: nacos-demo-group
        namespace: nacos-demo-namespace
      config:
        server-addr: localhost:8800/cluster/ # 配置中心
        group: nacos-demo-group
        namespace: nacos-demo-namespace
```

- 使用 **Nginx**

```json
# /usr/local/nginx/conf/nginx.conf

upstream cluster { # 自定义服务名
  server 127.0.0.1:8844;
  server 127.0.0.1:8846;
  server 127.0.0.1:8848;
}

server {
  listen       8800 # 监听端口
  server_name  localhost;

  location ~ /cluster/ {
    proxy_pass http://cluster/;
    # 由 localhost:8800 转发到 127.0.0.1:884x
  }
}
```

- 逐一启动各微服务

```bash
# 逐一启动 nacos-8844、nacos-8846、nacos-8848
$ startup.cmd -m cluster
$ ./startup.sh -m cluster # 或
```

- 测试用例

```bash
# 同单机版Nacos
```