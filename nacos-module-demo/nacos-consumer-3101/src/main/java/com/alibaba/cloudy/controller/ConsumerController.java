package com.alibaba.cloudy.controller;

import com.alibaba.cloudy.entities.Results;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@RequestMapping(value = "/consumer")
@RestController
@Slf4j
@RefreshScope // ! important
public class ConsumerController {

    // !important: Service names are case-sensitive
    private static final String PROVIDER_URL = "http://nacos-provider-service";

    @Value("${server.port}")
    private String serverPort;

    @Value("${profile.name}")
    private String profileName;

    @Value("${profile.version}")
    private String profileVersion;

    @Resource
    private RestTemplate restTemplate;

    @Resource
    private DiscoveryClient discoveryClient;

    @GetMapping("/product/get/{id}")
    public Results get(@PathVariable("id") Long id) {
        return restTemplate.getForObject(PROVIDER_URL + "/product/get/" + id, Results.class);
    }

    @GetMapping(value = "/profile/name")
    public Results<String> getName() {
        return new Results<>(200, profileName, String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @GetMapping(value = "/profile/version")
    public Results<String> getVersion() {
        return new Results<>(200, profileVersion, String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @GetMapping(value = "/discovery")
    public Results discovery() {
        List<String> infos = new ArrayList<>();

        List<String> services = discoveryClient.getServices();
        for (String service : services) {
            List<ServiceInstance> instances = discoveryClient.getInstances(service);
            for (ServiceInstance instance : instances) {
                infos.add(instance.getServiceId() + " " + instance.getUri());
            }
        }
        return new Results(200, infos, "SUCCESS");
    }

}
