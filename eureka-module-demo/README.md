# Netflix Eureka



### 1.Eureka 的组件说明

  Eureka是服务注册中心，分为Eureka Server和Client两个部分：

（1）Eureka Server：接受微服务节点的注册，留存注册信息（不参与通信转发）；

（2）Eureka Client：发起微服务节点的注册请求，维持与Server端的心跳检查，并内置客户端均衡负载Ribbon；

（3）Eureka 心跳检查：Client默认每30s向Server发送心跳包，Server超时等待90s后会自动移除Client的注册信息；



### 2. Eureka 的自我保护

  Eureka Server 超时等待来自Client的心跳包后，判定Client服务不可用，假定Client是因为通信不畅等原因导致的心跳失联（即Client本体是正常运行的），期待一段时间后网络通信能得以恢复，所以并不会立即剔除该Client的注册信息，这就是Eureka的自我保护。
	
  Eureka的自我保护是默认开启的，通过配置Server的application.yml `eureka.server.enable-self-preservation=false` 来取消自我保护模式，实现断连即删。
	
  开启后会在 Eureka Server主页 看到一串红色提示 `THE SELF PRESERVATION MODE IS TURNED OFF. THIS MAY NOT PROTECT INSTANCE EXPIRY IN CASE OF NETWORK/OTHER PROBLEMS.`



### 3. 搭建Eureka服务端

- 建立 `eureka-server-2201` 子Module

- 在子pom中追加

```xml
<!-- Eureka Server -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableEurekaServer` 注解

```java
@SpringBootApplication
@EnableEurekaServer
public class EurekaServer2201 {
    public static void main(String[] args) {
        SpringApplication.run(EurekaServer2201.class, args);
    }
}
```

- 在application.yml追加配置

```yml
server:
  port: 2201

eureka:
  instance:
    hostname: eureka-server-2201 # 自身实例名
  client:
    register-with-eureka: false # 是否将自己注册到 EurekaServer
    fetch-registry: false # 是否拉取注册表
    service-url:
      # defaultZone: http://eureka-server-2201:2201/eureka # 单机就是指向自己
      defaultZone: http://eureka-server-2022:2022/eureka/ # 集群就是指向其他
  server:
    enable-self-preservation: true # 关闭保护机制，服务一旦发生故障就立刻剔除
    eviction-interval-timer-in-ms: 30000 # 心跳包的超时间隔
```

- 启动并查看Eureka主页

```bash
# 访问 http://localhost:2201/
```

- 同样方法创建 `eureka-server-2202`

- 修改系统hosts

```bash
# 编辑 C:\Windows\System32\drivers\etc\hosts 并保存
127.0.0.1           eureka-server-2201
127.0.0.1           eureka-server-2202

# 刷新DNS
$ ipconfig /flushdns
```



### 4. 搭建Eureka客户端

##### （1）搭建2个业务实例 Provider

- 建立 `eureka-provider-2301` 子Module

- 在子pom中追加

```xml
<!-- Eureka Client -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableEurekaClient` 注解

```java
@SpringBootApplication
@EnableEurekaClient
public class EurekaProvider2301 {
    public static void main(String[] args) {
        SpringApplication.run(EurekaProvider2301.class, args);
    }
}
```

- 在application.yml追加配置

```yml
server:
  port: 2301

spring:
  application:
    name: eureka-provider-service
  datasource:
    type: com.alibaba.druid.pool.DruidDataSource
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/cloudy?useUnicode=true&characterEncoding=utf-8&useSSL=false&allowPublicKeyRetrieval=true
    username: root
    password: 123456

eureka:
  client:
    register-with-eureka: true # 是否将自己注册到 EurekaServer
    fetchRegistry: true # 是否拉取注册表（启用Ribbon均衡负载时必须开启）
    service-url:
      # defaultZone: http://eureka-server-2201:2201/eureka # 单机支持
      defaultZone: http://eureka-server-2201:2201/eureka,http://eureka-server-2202:2202/eureka # 集群支持
  instance:
    instance-id: eureka-provider-2301 # 实例名
    prefer-ip-address: true # 鼠标浮动在EurekaServer注册表中的实例名上时浏览器左下角动态显示本服务的实际IP
    lease-renewal-interval-in-seconds: 30 # EurekaClient发送心跳包到EurekaServer的时间间隔（默认30s），单位是秒
    lease-expiration-duration-in-seconds: 90 # EurekaServer对本服务的心跳超时的上限（默认90），超时剔除

mybatis-plus:
  configuration:
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl
  mapper-locations: classpath*:mapper/*.xml
  global-config:
    db-config:
      logic-not-delete-value: 0
      logic-delete-value: 1
```

- 编写Controller类

```java
@RestController
public class ProviderController {

    @Resource
    private ProductService productService;

    @Value("${server.port}")
    private String serverPort;

    @GetMapping(value = "/product/get/{id}")
    public Results<Product> getOne(@PathVariable("id") Long id) {
        Product product = productService.getById(id);
        if (null == product) {
            return new Results<>(404, null, String.format("FAIL(PORT=%s)", serverPort));
        }
        return new Results<>(200, product, String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @PostMapping(value = "/product/new")
    public Results<Product> newOne(@RequestBody Product product) {
        productService.saveOrUpdate(product);
        return new Results<>(200, product, String.format("SUCCESS(PORT=%s)", serverPort));
    }
}
```

- 同样方法创建 `eureka-provider-2302`

  注：维护 `eureka-provider-2301` 和 `eureka-provider-2302`的应用名 `spring.application.name` 相同，实例名 `eureka.instance.instance-id` 不同



##### （2）搭建1个业务实例 Consumer

- 建立 `eureka-consumer-2101` 子Module

- 在子pom中追加

```xml
<!-- Eureka Client -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableEurekaClient` 注解

```java
@SpringBootApplication
@EnableEurekaClient
public class EurekaConsumer2101 {
    public static void main(String[] args) {
        SpringApplication.run(EurekaConsumer2101.class, args);
    }
}
```

- 在application.yml追加配置

```yml
server:
  port: 2101

spring:
  application:
    name: eureka-consumer-service

eureka:
  client:
    register-with-eureka: true # 是否将自己注册到 EurekaServer
    fetchRegistry: true # 是否拉取注册表（启用Ribbon均衡负载时必须开启）
    service-url:
      # defaultZone: http://eureka-server-2201:2201/eureka # 单机支持
      defaultZone: http://eureka-server-2201:2201/eureka,http://eureka-server-2202:2202/eureka # 集群支持
  instance:
    instance-id: eureka-consumer-2101 # 实例名
    prefer-ip-address: true # 鼠标浮动在EurekaServer注册表中的实例名上时浏览器左下角动态显示本服务的实际IP
    lease-renewal-interval-in-seconds: 30 # EurekaClient发送心跳包到EurekaServer的时间间隔（默认30s），单位是秒
    lease-expiration-duration-in-seconds: 90 # EurekaServer对本服务的心跳超时的上限（默认90），超时剔除
```

- 添加RestTemplate配置
```java
@Configuration
public class ApplicationContextConfig {

    @Bean
    @LoadBalanced // !important
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}
```

- 编写Controller类

```java
@RequestMapping(value = "/consumer")
@RestController
public class ConsumerController {

    private static final String PROVIDER_URL = "http://EUREKA-PROVIDER-SERVICE";

    @Resource
    private RestTemplate restTemplate;

    @GetMapping("/product/get/{id}")
    public Results getOne(@PathVariable("id") Long id) {
        return restTemplate.getForObject(PROVIDER_URL + "/product/get/" + id, Results.class);
    }

    @PostMapping("/product/new")
    public Results newOne(@RequestBody Product product) {
        return restTemplate.postForObject(PROVIDER_URL + "/product/new", product, Results.class);
    }

}
```



### 5. 逐一启动各微服务

```bash
# 共5个微服务：eureka-server-2201（注册中心1）、eureka-server-2202（注册中心2）、eureka-provider-2301（微服务实例 生产者）、eureka-provider-2302（微服务实例 生产者）、eureka-consumer-2101（微服务实例 消费者）
```



### 6. 测试用例

```bash
# 访问 http://localhost:2201/ 查看注册列表和主备Eureka
> DS Replicas eureka-server-2202
> Instances currently registered with Eureka
> EUREKA-CONSUMER-SERVICE	n/a (1)	(1)	UP (1) - eureka-consumer-2101
> EUREKA-PROVIDER-SERVICE	n/a (2)	(2)	UP (2) - eureka-provider-2302 , eureka-provider-2301

# 访问 http://localhost:2202/ 查看注册列表和主备Eureka
> DS Replicas eureka-server-2201
> Instances currently registered with Eureka
> EUREKA-CONSUMER-SERVICE	n/a (1)	(1)	UP (1) - eureka-consumer-2101
> EUREKA-PROVIDER-SERVICE	n/a (2)	(2)	UP (2) - eureka-provider-2302 , eureka-provider-2301

# 测试服务间的通信和负载均衡
$ curl http://localhost:2101/consumer/product/get/1
> {"code":200,"data":{"id":1,"name":"book","price":30.0,"count":2},"message":"SUCCESS(PORT=2301)"}
$ curl http://localhost:2101/consumer/product/get/1
> {"code":200,"data":{"id":1,"name":"book","price":30.0,"count":2},"message":"SUCCESS(PORT=2302)"}

# 测试POST类型（建议用POSTMAN）
$ curl -H "Content-Type:application/json" -X POST http://localhost:2101/consumer/product/new -d '{"name":"phone","price":5000,"count":1}'
> {"code":200,"data":{"id":2,"name":"phone","price":5000.0,"count":1},"message":"SUCCESS(PORT=2301)"}
```



### 7. Eureka Client 服务发现

  Eureka Client 在自我注册的同时会获取一份完整的注册列表，并缓存到本地JVM，循环发起心跳包的同时更新本地保存的注册列表；Client可以在启用`@EnableDiscoveryClient`条件下使用`DiscoveryClient` 显式的获取注册列表。

- 在SpringBoot启动类追加 `@EnableDiscoveryClient` 注解

```java
// 任意微服务均可以
@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
public class EurekaConsumer2101 {
    public static void main(String[] args) {
        SpringApplication.run(EurekaConsumer2101.class, args);
    }
}
```

- 编写Controller类

```java
@RequestMapping(value = "/consumer")
@RestController
@Slf4j
public class ConsumerController {

    @Resource
    private DiscoveryClient discoveryClient;

    @GetMapping(value = "/discovery")
    public Results discovery() {
        List<String> infos = new ArrayList<>();

        List<String> services = discoveryClient.getServices();
        for (String service : services) {
            List<ServiceInstance> instances = discoveryClient.getInstances(service);
            for (ServiceInstance instance : instances) {
                infos.add(instance.getServiceId() + " " + instance.getUri());
            }
        }
        return new Results(200, infos, "SUCCESS");
    }

}
```

- 测试用例

```bash
$ curl http://localhost:2101/consumer/discovery
> {"code":200,"data":["EUREKA-PROVIDER-SERVICE http://192.168.1.1:2302","EUREKA-PROVIDER-SERVICE http://192.168.1.1:2301","EUREKA-CONSUMER-SERVICE http://192.168.1.1:2101"],"message":"SUCCESS"}
```
