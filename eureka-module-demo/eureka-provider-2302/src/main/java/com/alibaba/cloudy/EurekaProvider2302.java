package com.alibaba.cloudy;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@Slf4j
@EnableEurekaClient
public class EurekaProvider2302 {

    public static void main(String[] args) {
        SpringApplication.run(EurekaProvider2302.class, args);
        log.info("(づ￣ 3￣)づ(づ￣ 3￣)づ(づ￣ 3￣)づ(づ￣ 3￣)づΨ(￣∀￣)ΨΨ(￣∀￣)ΨΨ(￣∀￣)Ψ");
    }

}
