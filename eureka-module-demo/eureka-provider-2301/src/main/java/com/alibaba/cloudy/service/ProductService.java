package com.alibaba.cloudy.service;

import com.alibaba.cloudy.entities.Product;
import com.baomidou.mybatisplus.extension.service.IService;

public interface ProductService extends IService<Product> {

    public Product getByName(String name);

    public Product reduce(String name, Integer count);

}
