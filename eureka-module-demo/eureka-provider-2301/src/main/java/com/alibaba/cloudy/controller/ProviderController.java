package com.alibaba.cloudy.controller;

import com.alibaba.cloudy.entities.Product;
import com.alibaba.cloudy.entities.Results;
import com.alibaba.cloudy.service.ProductService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
public class ProviderController {

    @Resource
    private ProductService productService;

    @Value("${server.port}")
    private String serverPort;

    @GetMapping(value = "/product/get/{id}")
    public Results<Product> getOne(@PathVariable("id") Long id) {
        Product product = productService.getById(id);
        if (null == product) {
            return new Results<>(404, null, String.format("FAIL(PORT=%s)", serverPort));
        }
        return new Results<>(200, product, String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @PostMapping(value = "/product/new")
    public Results<Product> newOne(@RequestBody Product product) {
        productService.saveOrUpdate(product);
        return new Results<>(200, product, String.format("SUCCESS(PORT=%s)", serverPort));
    }
}
