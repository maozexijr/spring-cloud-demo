package com.alibaba.cloudy;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@Slf4j
@EnableEurekaClient
@EnableDiscoveryClient
public class EurekaConsumer2101 {

    public static void main(String[] args) {
        SpringApplication.run(EurekaConsumer2101.class, args);
        log.info("Ψ(￣∀￣)ΨΨ(￣∀￣)ΨΨ(￣∀￣)Ψ(づ￣ 3￣)づ(づ￣ 3￣)づ(づ￣ 3￣)づ(づ￣ 3￣)づ");
    }

}
