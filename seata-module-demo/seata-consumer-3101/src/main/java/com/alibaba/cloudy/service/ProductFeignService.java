package com.alibaba.cloudy.service;

import com.alibaba.cloudy.entities.Product;
import com.alibaba.cloudy.entities.Results;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

import java.util.List;

@Component
@FeignClient(value = "seata-provider-service")
// !important: Service names are case-sensitive
public interface ProductFeignService {

    @GetMapping(value = "/product/list")
    public Results<List<Product>> listAll();

    @GetMapping(value = "/product/get/{name}")
    public Results<Product> getOne(@PathVariable("name") String name);

    @PutMapping(value = "/{name}/reduce/{count}")
    public Results<Product> reduce(@PathVariable("name") String name, @PathVariable("count") Integer count);

}
