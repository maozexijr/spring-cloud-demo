package com.alibaba.cloudy.service.impl;

import com.alibaba.cloudy.entities.Account;
import com.alibaba.cloudy.mapper.AccountMapper;
import com.alibaba.cloudy.service.AccountService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements AccountService {

    @Resource
    private AccountMapper accountMapper;

    @Override
    public Account getByName(String user) {
        QueryWrapper<Account> query = new QueryWrapper<>();
        query.lambda().eq(Account::getName, user);
        return accountMapper.selectOne(query);
    }

    @Override
    public Account reduce(String user, Double money) {
        Account account = getByName(user);
        account.reduce(money);
        accountMapper.updateById(account);
        return account;
    }

}
