package com.alibaba.cloudy.service;

import com.alibaba.cloudy.entities.Account;
import com.baomidou.mybatisplus.extension.service.IService;

public interface AccountService extends IService<Account> {

    public Account getByName(String user);

    public Account reduce(String user, Double money);

}
