package com.alibaba.cloudy.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.cloudy.entities.Account;
import com.alibaba.cloudy.entities.Product;
import com.alibaba.cloudy.entities.Results;
import com.alibaba.cloudy.service.AccountService;
import com.alibaba.cloudy.service.ProductFeignService;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RequestMapping(value = "/consumer")
@RestController
@Slf4j
public class ConsumerController {

    @Resource
    private AccountService accountService;

    @Resource
    private ProductFeignService productFeignService;

    @GetMapping("/account/list")
    public Results<List<Account>> listAll() {
        List<Account> accounts = accountService.list();
        if (CollUtil.isEmpty(accounts)) {
            return new Results(404, null, "FAIL");
        }
        return new Results(200, accounts, "SUCCESS");
    }

    @GetMapping("/product/list")
    public Results<List<Product>> listProduct() {
        Results<List<Product>> results = productFeignService.listAll();
        if (!results.payload()) {
            return results;
        }
        return new Results(200, results.getData(), "SUCCESS");
    }

    @PutMapping("/{user}/buy/{count}/{product}")
    @GlobalTransactional
    public Results buyProduct(
            @PathVariable("user") String user,
            @PathVariable("count") Integer count,
            @PathVariable("product") String product
    ) {
        log.info("Query the price of {}", product);
        Results<Product> results = productFeignService.getOne(product);
        if (!results.payload()) {
            return results;
        }

        log.info(JSONUtil.toJsonStr(results));
        Double money = results.getData().getPrice() * count;

        log.info("Check the balance of {}'s account", user);
        Account account = accountService.getByName(user);
        log.info(JSONUtil.toJsonStr(account));

        log.info("{} is paying {} for {}", user, money, product);
        account.reduce(money);
        accountService.saveOrUpdate(account);
        log.info("Payment has been completed");

        productFeignService.reduce(product, count);
        return new Results(200, account, "SUCCESS");
    }

}
