package com.alibaba.cloudy.controller;

import com.alibaba.cloudy.entities.Product;
import com.alibaba.cloudy.entities.Results;
import com.alibaba.cloudy.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@Slf4j
public class ProviderController {

    @Resource
    private ProductService productService;

    @Value("${server.port}")
    private String serverPort;

    @GetMapping(value = "/product/list")
    public Results<List<Product>> listAll() {
        List<Product> list = productService.list();
        return new Results<>(200, list, String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @GetMapping(value = "/product/get/{name}")
    public Results<Product> getOne(@PathVariable("name") String name) {
        Product product = productService.getByName(name);
        if (null == product) {
            return new Results<>(404, null, String.format("FAIL(PORT=%s)", serverPort));
        }
        return new Results<>(200, product, String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @PostMapping(value = "/product/new")
    public Results<Product> newOne(@RequestBody Product product) {
        log.info("RECEIVE {}", product);
        productService.saveOrUpdate(product);
        return new Results<>(200, product, String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @PutMapping(value = "/{name}/reduce/{count}")
    public Results<Product> reduce(@PathVariable("name") String name, @PathVariable("count") Integer count) {
        Product product = productService.getByName(name);
        if (null == product) {
            return new Results<>(404, null, String.format("FAIL(PORT=%s)", serverPort));
        }

        log.info("Reducing the count of {}", name);
        product.reduce(count);
        productService.saveOrUpdate(product);
        log.info("Reduce has been completed");

        return new Results<>(200, product, String.format("SUCCESS(PORT=%s)", serverPort));
    }
}
