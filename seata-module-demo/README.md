# Alibaba Seata



### 1. Seata

  官网：https://seata.io/zh-cn/docs/overview/what-is-seata.html

  Seata 是分布式事务解决方案，提供了 `AT` 、`TCC` 、`SAGA` 和 `XA` 4种事务模式，默认使用低耦合、无侵入的 `AT` 模式。


  为实现分布式事务，Seata 引入了  `TC` 、 `TM` 、 `RM` 3个概念：

- TC （Transaction Coordinator、事务协调者）

  全局事务和分支事务的管理者、协调者，审批全局事务和分支事务的创建，响应事务的提交或回滚请求；

- TM （Transaction Manager、全局事务管理器）

  全局事务创建的发起者，全局事务提交或回滚的发起者；

- RM （Resource Manager、分支事务管理器、资源管理器）

  分支事务创建的发起者，分支事务提交或回滚的执行者；


  AT模式的执行流程：

（1）TM向TC申请创建一个全局事务，生成全局唯一的事务id（XID）；

（2）XID在微服务链路上广泛传播；

（3）RM向TC申请创建一个归于XID的分支事务；

（4）TM向TC发起针对XID的全局提交或回滚请求；

（5）TC调度XID下的所有分支事务完成全局提交或回滚；



### 2. 搭建Nacos服务端

- 下载

```bash
# https://github.com/alibaba/nacos/tags
```

- 启动

```bash
# 程序文件所在bin路径下执行
$ startup.cmd -m standalone
$ ./startup.sh -m standalone # 或
```

- 访问

```bash
# http://localhost:8848/nacos
# 默认账密 nacos/nacos
```



### 3. 搭建Seata服务端

- 下载

```bash
# https://github.com/seata/seata/releases
# 分别下载 seata-server-0.9.0 和 seata-server-1.4.2
```

- 脚本准备

```bash
# 分别解压 seata-server-0.9.0 和 seata-server-1.4.2

# 拷贝0.9.0/conf内的以下几个脚步到1.4.2 
# (1) nacos-config.txt
# (2) nacos-config.sh
# (3) nacos-config.py
# (4) db_store.sql
# (5) db_undo_log.sql

# 删除1.4.2/conf中的file.conf
# 拷贝file.conf.example重命名为file.conf
```

- 修改配置

```bash
# 修改 1.4.2/conf/file.conf

# store.mode = "db"
# store.db.driverClassName = "com.mysql.cj.jdbc.Driver"
# store.db.url = "jdbc:mysql://127.0.0.1:3306/seata?rewriteBatchedStatements=true&allowPublicKeyRetrieval=true&serverTimezone=Asia/Shanghai"
# store.db.user = "root"
# store.db.password = "123456"
```

```bash
# 修改 1.4.2/conf/registry.conf

# registry.type = "nacos"
# config.type = "nacos"

# 注意 nacos.group和nacos.namespace如需修改，应与Seata客户端application.xml同步修改
```


- 建库建表

```bash
# 创建名为`seata`的数据库
# 使用 1.4.2/conf/db_store.sql脚本在`seata`库内建表
# 使用 1.4.2/conf/db_undo_log.sql脚本在各微服务实例数据库分别建表（一库一表）
```

- 配置列表

```bash
# 在启动nacos情况下执行脚本1.4.2/conf/nacos-config.sh或nacos-config.py
$ ./nacos-config.sh 127.0.0.1
$ python nacos-config.py 127.0.0.1 # 或

# 访问Nacos配置中心 http://localhost:8848/nacos
# 配置管理 > 配置列表 > public（命名空间）

# 克隆配置参数 service.vgroup_mapping.my_test_tx_group 新命名为
# service.vgroupMapping.my_test_tx_group
```

- 启动

```bash
# 程序文件所在bin路径下执行
$ seata-server.bat
$ ./seata-server.sh # 或
```



### 4. 搭建Seata客户端

##### （1）搭建业务实例 Provider

- 建立 `seata-provider-3301` 子Module

- 在子pom中追加

```xml
<!-- Seata -->
<dependency>
    <groupId>io.seata</groupId>
    <artifactId>seata-spring-boot-starter</artifactId>
    <version>1.4.2</version>
</dependency>
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-seata</artifactId>
    <exclusions>
        <exclusion>
            <groupId>io.seata</groupId>
            <artifactId>seata-spring-boot-starter</artifactId>
        </exclusion>
    </exclusions>
</dependency>

<!-- Nacos -->
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
</dependency>
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableDiscoveryClient` 注解

```java
@SpringBootApplication
@EnableDiscoveryClient
public class SeataProvider3301 {
    public static void main(String[] args) {
        SpringApplication.run(SeataProvider3301.class, args);
    }
}
```

- 在application.yml追加配置

```yml
server:
  port: 3301

spring:
  application:
    name: seata-provider-service
  datasource:
    #type: com.alibaba.druid.pool.DruidDataSource
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/cloudy?useUnicode=true&characterEncoding=utf-8&useSSL=false&allowPublicKeyRetrieval=true
    username: root
    password: 123456
  cloud:
    nacos:
      discovery:
        server-addr: localhost:8848 # 注册中心
        group: SEATA_GROUP
        namespace:
      config:
        server-addr: localhost:8848 # 配置中心
        file-extension: yaml # 配置文件格式，支持 yaml、xml、properties、json、text、html
        group: SEATA_GROUP
        namespace:

seata:
  enabled: true
  enableAutoDataSourceProxy: true
  tx-service-group: my_test_tx_group
  registry:
    type: nacos
    nacos:
      application: seata-server
      server-addr: 127.0.0.1:8848
      username: nacos
      password: nacos
  config:
    type: nacos
    nacos:
      server-addr: 127.0.0.1:8848
      username: nacos
      password: nacos
      group: SEATA_GROUP
      namespace:

mybatis-plus:
  configuration:
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl
  mapper-locations: classpath*:mapper/*.xml
  global-config:
    db-config:
      logic-not-delete-value: 0
      logic-delete-value: 1

management:
  endpoints:
    web:
      exposure:
        include: "*" # 暴露/actuator
```

- 编写配置类

```java
@Configuration
public class DataSourceConfig {
    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource druidDataSource() {
        return new DruidDataSource();
    }
}
```

- 编写Controller类

```java
@RestController
@Slf4j
public class ProviderController {

    @Resource
    private ProductService productService;

    @Value("${server.port}")
    private String serverPort;

    @GetMapping(value = "/product/list")
    public Results<List<Product>> listAll() {
        List<Product> list = productService.list();
        return new Results<>(200, list, String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @GetMapping(value = "/product/get/{name}")
    public Results<Product> getOne(@PathVariable("name") String name) {
        Product product = productService.getByName(name);
        if (null == product) {
            return new Results<>(404, null, String.format("FAIL(PORT=%s)", serverPort));
        }
        return new Results<>(200, product, String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @PostMapping(value = "/product/new")
    public Results<Product> newOne(@RequestBody Product product) {
        log.info("RECEIVE {}", product);
        productService.saveOrUpdate(product);
        return new Results<>(200, product, String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @PutMapping(value = "/{name}/reduce/{count}")
    public Results<Product> reduce(@PathVariable("name") String name, @PathVariable("count") Integer count) {
        Product product = productService.getByName(name);
        if (null == product) {
            return new Results<>(404, null, String.format("FAIL(PORT=%s)", serverPort));
        }

        log.info("Reducing the count of {}", name);
        product.reduce(count);
        productService.saveOrUpdate(product);
        log.info("Reduce has been completed");

        return new Results<>(200, product, String.format("SUCCESS(PORT=%s)", serverPort));
    }
    
}
```


##### （2）搭建业务实例 Consumer

- 建立 `seata-consumer-3101` 子Module

- 在子pom中追加

```xml
<!-- Seata -->
<dependency>
    <groupId>io.seata</groupId>
    <artifactId>seata-spring-boot-starter</artifactId>
    <version>1.4.2</version>
</dependency>
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-seata</artifactId>
    <exclusions>
        <exclusion>
            <groupId>io.seata</groupId>
            <artifactId>seata-spring-boot-starter</artifactId>
        </exclusion>
    </exclusions>
</dependency>

<!-- Nacos -->
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
</dependency>
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
</dependency>

<!-- Feign -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-openfeign</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableDiscoveryClient` 和`@EnableFeignClients`注解

```java
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class SeataConsumer3101 {
    public static void main(String[] args) {
        SpringApplication.run(SeataConsumer3101.class, args);
    }
}
```

- 在application.yml追加配置

```yml
server:
  port: 3101

spring:
  application:
    name: seata-consumer-service
  datasource:
    #type: com.alibaba.druid.pool.DruidDataSource
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/cloudy?useUnicode=true&characterEncoding=utf-8&useSSL=false&allowPublicKeyRetrieval=true
    username: root
    password: 123456
  cloud:
    nacos:
      discovery:
        server-addr: localhost:8848 # 注册中心
        group: SEATA_GROUP
        namespace:
      config:
        server-addr: localhost:8848 # 配置中心
        file-extension: yaml # 配置文件格式，支持 yaml、xml、properties、json、text、html
        group: SEATA_GROUP
        namespace:

seata:
  enabled: true
  enableAutoDataSourceProxy: true
  tx-service-group: my_test_tx_group
  registry:
    type: nacos
    nacos:
      application: seata-server
      server-addr: 127.0.0.1:8848
      username: nacos
      password: nacos
  config:
    type: nacos
    nacos:
      server-addr: 127.0.0.1:8848
      username: nacos
      password: nacos
      group: SEATA_GROUP
      namespace:

mybatis-plus:
  configuration:
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl
  mapper-locations: classpath*:mapper/*.xml
  global-config:
    db-config:
      logic-not-delete-value: 0
      logic-delete-value: 1

ribbon:
  ConnectTimeout: 2000 # 建立连接所支持的最大耗时，单位毫秒
  ReadTimeout: 5000 # 读取响应内容所支持的最大耗时

logging:
  level:
    com.alibaba.cloudy.service.ProductFeignService: debug # 指定日志对象

management:
  endpoints:
    web:
      exposure:
        include: "*" # 暴露/actuator
```

- 编写Service层并追加`@FeignClient`注解

```java
@Component
@FeignClient(value = "seata-provider-service")
// !important: Service names are case-sensitive
public interface ProductFeignService {

    @GetMapping(value = "/product/list")
    public Results<List<Product>> listAll();

    @GetMapping(value = "/product/get/{name}")
    public Results<Product> getOne(@PathVariable("name") String name);

    @PutMapping(value = "/{name}/reduce/{count}")
    public Results<Product> reduce(@PathVariable("name") String name, @PathVariable("count") Integer count);

}
```

- 编写Controller类

```java
@RequestMapping(value = "/consumer")
@RestController
@Slf4j
public class ConsumerController {

    @Resource
    private AccountService accountService;

    @Resource
    private ProductFeignService productFeignService;

    @GetMapping("/account/list")
    public Results<List<Account>> listAll() {
        List<Account> accounts = accountService.list();
        if (CollUtil.isEmpty(accounts)) {
            return new Results(404, null, "FAIL");
        }
        return new Results(200, accounts, "SUCCESS");
    }

    @GetMapping("/product/list")
    public Results<List<Product>> listProduct() {
        Results<List<Product>> results = productFeignService.listAll();
        if (!results.payload()) {
            return results;
        }
        return new Results(200, results.getData(), "SUCCESS");
    }

    @PutMapping("/{user}/buy/{count}/{product}")
    @GlobalTransactional
    public Results buyProduct(
            @PathVariable("user") String user,
            @PathVariable("count") Integer count,
            @PathVariable("product") String product
    ) {
        log.info("Query the price of {}", product);
        Results<Product> results = productFeignService.getOne(product);
        if (!results.payload()) {
            return results;
        }

        log.info(JSONUtil.toJsonStr(results));
        Double money = results.getData().getPrice() * count;

        log.info("Check the balance of {}'s account", user);
        Account account = accountService.getByName(user);
        log.info(JSONUtil.toJsonStr(account));

        log.info("{} is paying {} for {}", user, money, product);
        account.reduce(money);
        accountService.saveOrUpdate(account);
        log.info("Payment has been completed");

        productFeignService.reduce(product, count);
        return new Results(200, account, "SUCCESS");
    }

}
```



### 5. 逐一启动各微服务

```bash
# 逐一启动 nacos-server-8848（注册中心和配置中心）、seata-server-8091（事务TC）、seata-provider-3301（微服务实例 生产者）、seata-consumer-3101（微服务实例 消费者）
```



### 6. 测试用例

```bash
# 测试生产者
$ curl http://localhost:3301/product/list
> {"code":200,"data":[{"id":1,"name":"book","price":30.0,"count":2}],"message":"SUCCESS(PORT=3301)"}

# 测试消费者
$ curl http://localhost:3101/consumer/account/list
> {"code":200,"data":[{"id":1,"name":"admin","money":100.0}],"message":"SUCCESS"}

# 测试事务回滚
$ curl -X PUT http://localhost:3101/consumer/admin/buy/3/book
> [500] during [PUT] to [http://seata-provider-service/book/reduce/3] [ProductFeignService#reduce(String,Integer)]

# 测试正常消费
$ curl -X PUT http://localhost:3101/consumer/admin/buy/2/book
> {"code":200,"data":{"id":1,"name":"admin","money":40.0},"message":"SUCCESS"}
```
