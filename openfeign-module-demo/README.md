# OpenFeign



### 1. OpenFeign

  OpenFeign是开源社区为了解决Netflix Feign停更、断档的困境而开发的，是Netflix Feign的拓展，主要功能是实现Restful风格的跨服务请求。



### 2. 搭建Consul服务端

- 下载

```bash
# https://www.consul.io/downloads
```

- 安装与启动

```bash
# 双击安装
# 开发模式启动（程序文件所在路径下执行）
$ consul agent -dev
```

- 访问

```bash
# http://localhost:8500
# 免密登录
```



### 3. 搭建Consul客户端

##### （1）搭建2个业务实例 Provider

- 建立 `openfeign-provider-2301` 子Module

- 在子pom中追加

```xml
<!-- Consul -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-consul-discovery</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableDiscoveryClient` 注解

```java
@SpringBootApplication
@EnableDiscoveryClient
public class OpenFeignProvider2301 {
    public static void main(String[] args) {
        SpringApplication.run(OpenFeignProvider2301.class, args);
    }
}
```

- 在application.yml追加配置

```yml
server:
  port: 2301

spring:
  application:
    name: openfeign-provider-service
  datasource:
    type: com.alibaba.druid.pool.DruidDataSource
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/cloudy?useUnicode=true&characterEncoding=utf-8&useSSL=false&allowPublicKeyRetrieval=true
    username: root
    password: 123456
  cloud:
    consul:
      host: localhost
      port: 8500
      discovery:
        service-name: ${spring.application.name}

mybatis-plus:
  configuration:
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl
  mapper-locations: classpath*:mapper/*.xml
  global-config:
    db-config:
      logic-not-delete-value: 0
      logic-delete-value: 1
```

- 编写Controller类

```java
@RestController
@Slf4j
public class ProviderController {

    @Resource
    private ProductService productService;

    @Value("${server.port}")
    private String serverPort;

    @GetMapping(value = "/product/get/{id}")
    public Results<Product> getOne(@PathVariable("id") Long id) {
        Product product = productService.getById(id);
        if (null == product) {
            return new Results<>(404, null, String.format("FAIL(PORT=%s)", serverPort));
        }
        return new Results<>(200, product, String.format("SUCCESS(PORT=%s)", serverPort));
    }

}
```

- 同样方法创建 `openfeign-provider-2302`



##### （2）搭建1个业务实例 Consumer

- 建立 `openfeign-consumer-2101` 子Module

- 在子pom中追加

```xml
<!-- Feign -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-openfeign</artifactId>
</dependency>

<!-- Consul -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-consul-discovery</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableDiscoveryClient` 和`@EnableFeignClients`注解

```java
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class OpenFeignConsumer2101 {
    public static void main(String[] args) {
        SpringApplication.run(OpenFeignConsumer2101.class, args);
    }
}
```

- 在application.yml追加配置

```yml
server:
  port: 2101

spring:
  application:
    name: openfeign-consumer-service
  cloud:
    consul:
      host: localhost
      port: 8500
      discovery:
        service-name: ${spring.application.name}
```

- 编写Service层，在Service接口追加`@FeignClient`注解

```java
@Component
@FeignClient(value = "OPENFEIGN-PROVIDER-SERVICE")
public interface ProductFeignService {
    @GetMapping(value = "/product/get/{id}")
    public Results<Product> getOne(@PathVariable("id") Long id);
}
```

- 编写Controller类

```java
@RequestMapping(value = "/consumer")
@RestController
@Slf4j
public class ConsumerController {

    @Resource
    private ProductFeignService productFeignService;

    @GetMapping("/product/get/{id}")
    public Results getOne(@PathVariable("id") Long id) {
        return productFeignService.getOne(id);
    }
}
```



### 4. 逐一启动各微服务

```bash
# 逐一启动 consul-server-8500.bat（注册中心）、openfeign-provider-2301（微服务实例 生产者）、openfeign-provider-2302（微服务实例 生产者）、openfeign-consumer-2101（微服务实例 消费者）
```



### 5. 测试用例（通信）

```bash
# 测试服务间的通信
$ curl http://localhost:2101/consumer/product/get/1
> {"code":200,"data":{"id":1,"name":"book","price":30.0,"count":2},"message":"SUCCESS(PORT=2301)"}
```



### 6. Ribbon的请求超时

- 在Provider的Controller层追加方法

```java
@Value("${server.port}")
private String serverPort;

@GetMapping(value = "/product/timeout/{seconds}")
public Results<String> timeout(@PathVariable("seconds") Long seconds) {
    ThreadUtil.sleep(seconds * 1000);
    return new Results<String>(200, null, String.format("SUCCESS(PORT=%s)", serverPort));
}
```

- 在Consumer的Service层追加方法

```java
@GetMapping(value = "/product/timeout/{seconds}")
public Results<Long> timeout(@PathVariable("seconds") Long seconds);
```

- 在Consumer的application.yml追加配置

```yml
ribbon:
  ConnectTimeout: 2000 # 建立连接所支持的最大耗时，单位毫秒
  ReadTimeout: 5000 # 读取响应内容所支持的最大耗时
```

- 在Consumer的Controller层追加方法

```java
@Value("${ribbon.ConnectTimeout}")
private Long ribbonConnectTimeout;

@Value("${ribbon.ReadTimeout}")
private Long ribbonReadTimeout;

@GetMapping("/product/timeout/{seconds}")
public Results timeout(@PathVariable("seconds") Long seconds) {
    Results results = productFeignService.timeout(seconds);
    results.setData(String.format("ConnectTimeout=%ss, ReadTimeout=%ss", ribbonConnectTimeout / 1000, ribbonReadTimeout / 1000));
    return results;
}
```

- 测试Ribbon的请求超时

```bash
# 正常响应
$ curl http://localhost:2101/consumer/product/timeout/2
> {"code":200,"data":"ConnectTimeout=2s, ReadTimeout=5s","message":"SUCCESS(PORT=2301)"}

# 超时报错
$ curl http://localhost:2101/consumer/product/timeout/5
> There was an unexpected error (type=Internal Server Error, status=500).
> Read timed out executing GET http://OPENFEIGN-PROVIDER-SERVICE/product/timeout/5
> feign.RetryableException: Read timed out executing GET http://OPENFEIGN-PROVIDER-SERVICE/product/timeout/5
```



### 7. 修改Feign日志级别

- 在Consumer中添加配置类

```java
@Configuration
public class OpenFeignConfig {
    @Bean
    Logger.Level feignLoggerLevel() {
        //
        return Logger.Level.FULL;
    }
}
```

- 在Consumer的application.yml追加配置

```yml
logging:
  level:
    com.alibaba.cloudy.service.ProductFeignService: debug # 指定日志对象
```

- 测试Feign的日志输出

```bash
$ curl http://localhost:2101/consumer/product/get/1

# DEBUG 12724 --- [nio-2101-exec-1] c.a.cloudy.service.ProductFeignService : [ProductFeignService#getOne] ---> GET http://OPENFEIGN-PROVIDER-SERVICE/product/get/1 HTTP/1.1
# DEBUG 12724 --- [nio-2101-exec-1] c.a.cloudy.service.ProductFeignService : [ProductFeignService#getOne] ---> END HTTP (0-byte body)
# DEBUG 12724 --- [nio-2101-exec-1] c.a.cloudy.service.ProductFeignService : [ProductFeignService#getOne] <--- HTTP/1.1 200 (6ms)
# DEBUG 12724 --- [nio-2101-exec-1] c.a.cloudy.service.ProductFeignService : [ProductFeignService#getOne] connection: keep-alive
# DEBUG 12724 --- [nio-2101-exec-1] c.a.cloudy.service.ProductFeignService : [ProductFeignService#getOne] content-type: application/json
# DEBUG 12724 --- [nio-2101-exec-1] c.a.cloudy.service.ProductFeignService : [ProductFeignService#getOne] date: Sat, 21 Aug 2021 13:54:34 GMT
# DEBUG 12724 --- [nio-2101-exec-1] c.a.cloudy.service.ProductFeignService : [ProductFeignService#getOne] keep-alive: timeout=60
# DEBUG 12724 --- [nio-2101-exec-1] c.a.cloudy.service.ProductFeignService : [ProductFeignService#getOne] transfer-encoding: chunked
# DEBUG 12724 --- [nio-2101-exec-1] c.a.cloudy.service.ProductFeignService : [ProductFeignService#getOne] 
# DEBUG 12724 --- [nio-2101-exec-1] c.a.cloudy.service.ProductFeignService : [ProductFeignService#getOne] {"code":200,"data":{"id":1,"name":"book","price":30.0,"count":2},"message":"SUCCESS(PORT=2302)"}
# DEBUG 12724 --- [nio-2101-exec-1] c.a.cloudy.service.ProductFeignService : [ProductFeignService#getOne] <--- END HTTP (96-byte body)
```