package com.alibaba.cloudy.controller;

import com.alibaba.cloudy.entities.Product;
import com.alibaba.cloudy.entities.Results;
import com.alibaba.cloudy.service.ProductFeignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@RequestMapping(value = "/consumer")
@RestController
@Slf4j
public class ConsumerController {

    @Resource
    private ProductFeignService productFeignService;

    @Resource
    private DiscoveryClient discoveryClient;

    @Value("${ribbon.ConnectTimeout}")
    private Long ribbonConnectTimeout;

    @Value("${ribbon.ReadTimeout}")
    private Long ribbonReadTimeout;

    @GetMapping("/product/get/{id}")
    public Results getOne(@PathVariable("id") Long id) {
        return productFeignService.getOne(id);
    }

    @PostMapping("/product/new")
    public Results newOne(@RequestBody Product product) {
        return productFeignService.newOne(product);
    }

    @GetMapping(value = "/discovery")
    public Results discovery() {
        List<String> infos = new ArrayList<>();

        List<String> services = discoveryClient.getServices();
        for (String service : services) {
            List<ServiceInstance> instances = discoveryClient.getInstances(service);
            for (ServiceInstance instance : instances) {
                infos.add(instance.getServiceId() + " " + instance.getUri());
            }
        }
        return new Results(200, infos, "SUCCESS");
    }

    @GetMapping("/product/timeout/{seconds}")
    public Results timeout(@PathVariable("seconds") Long seconds) {
        Results result = productFeignService.timeout(seconds);
        result.setData(String.format("ConnectTimeout=%ss, ReadTimeout=%ss", ribbonConnectTimeout / 1000, ribbonReadTimeout / 1000));
        return result;
    }

}
