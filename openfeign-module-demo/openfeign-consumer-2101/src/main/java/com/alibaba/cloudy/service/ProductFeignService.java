package com.alibaba.cloudy.service;

import com.alibaba.cloudy.entities.Product;
import com.alibaba.cloudy.entities.Results;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Component
@FeignClient(value = "OPENFEIGN-PROVIDER-SERVICE")
public interface ProductFeignService {

    @GetMapping(value = "/product/get/{id}")
    public Results<Product> getOne(@PathVariable("id") Long id);

    @PostMapping(value = "/product/new")
    public Results<Product> newOne(@RequestBody Product product);

    @GetMapping(value = "/product/timeout/{seconds}")
    public Results<Long> timeout(@PathVariable("seconds") Long seconds);

}
