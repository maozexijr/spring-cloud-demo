# Spring Cloud Demo



### 1. 分布式CAP原则

```
一致性(Consistency)、可用性(Availability)、分区容错性(Partition tolerance)

分布式系统必须支持分区容错(P)，能且只能同时满足其中两个原则，要么AP原则，要么CP原则

Eureka是AP原则，保证高可用
Consul/Zookeeper是CP原则，保证高准确
Nacos默认AP原则，可选配置为CP原则
```


### 2. 技术概览

- 服务注册与发现 Eureka/Consul/Zookeeper/**Nacos**

- 均衡负载 **Ribbon**/LoadBalancer

- 服务调用 Fegin/**Open Fegin**

- 服务降级、熔断、限流 Hystrix/**Sentinel**

- 服务网关 Zuul/**Gateway**

- 服务的配置中心 Config/**Nacos**

- 消息总线 Bus/**Nacos**

- 消息队列 **Stream**

- 全链路追踪 **Sleuth**

- 分布式事务 **Seata**



### 3. 官方文档

- 官方链接 https://spring.io/projects/spring-cloud

- 中文翻译 https://www.bookstack.cn/read/spring-cloud-docs/docs-index.md



### 4. 版本选择

- 依据 https://spring.io/projects/spring-cloud 页 `Release train Spring Boot compatibility` 提供版本参考

- 更详细的版本参考 https://start.spring.io/actuator/info

- 通过 https://spring.io/projects/spring-cloud#learn 跳转**GA**版**Reference Doc.**查看最佳推荐



### 5. 项目准备

- 约定 > 配置 > 编码

- 字符编码
```
File > Settings > Editor > File Encodings > UTF-8
```

- 注解生效
```
File > Settings > Build,Execution,Deployment > Compiler > Annotation Processors > Enable annotation processing
```

- Java版本
```
File > Settings > Build,Execution,Deployment > Compiler > Java Compiler > Per-module bytecode version > 8
```

- 文件过滤
```
File > Settings > Editor > File Types > Ignored Files and Folders > *.idea;*.iml
```



### 6. Lumbook支持

- 在项目pom中追加

```xml
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <version>1.18.20</version>
    <optional>true</optional>
</dependency>
```

- 在Plugins中安装
```xml
File > Settings > Plugins
```



### 7. DevTools支持

- 在父pom中追加
```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
            <version>2.5.3</version>
            <configuration>
                <fork>true</fork>
                <addResources>true</addResources>
            </configuration>
        </plugin>
    </plugins>
</build>
```

- 在子pom中追加
```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-devtools</artifactId>
    <scope>runtime</scope>
    <optional>true</optional>
</dependency>
```

- 修改编译项
```
File > Settings > Build,Execution,Deployment > Compiler
✓ Automatically show first error in editor
✓ Display notification on build completion
✓ Build prject automatically
✓ Compile independent modules in parallel
```

- 修改注册项
```
Ctrl + Alt + Shift + / > Registry
✓ actionSystem.assertFocusAccessFromEdt
✓ compiler.automake.allow.when.app.running
```

- 重启IDEA



### 8. RunDashboard支持

- 补充配置

```xml
<!-- /.idea/workspace.xml -->
<component name="RunDashboard"> 
  <option name="configurationTypes"> 
    <set> 
      <option value="SpringBootApplicationConfigurationType"/> 
    </set> 
  </option> 
</component>
```

- 重启IDEA

