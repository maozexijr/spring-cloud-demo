# Alibaba Sentinel



### 1. Sentinel

  官网 https://github.com/alibaba/Sentinel/wiki/介绍
  Sentinel的主要功能是服务降级、服务熔断、服务限流和服务监控。



### 2. 搭建Nacos服务

- 下载

```bash
# https://github.com/alibaba/nacos/tags
```

- 启动

```bash
# 程序文件所在bin路径下执行
$ startup.cmd -m standalone
$ ./startup.sh -m standalone # 或
```

- 访问

```bash
# http://localhost:8848/nacos
# 默认账密 nacos/nacos
```



### 3. 搭建Sentinel服务

- 下载

```bash
# https://github.com/alibaba/Sentinel/releases
```

- 启动

```bash
# 在程序文件bin路径下执行
$ java -jar sentinel-dashboard-1.8.2.jar
```

- 访问

```bash
# http://localhost:8080
# 默认账密 sentinel/sentinel
```



### 4. 搭建业务实例 Provider

- 建立 `sentinel-provider-3301` 子Module

- 在子pom中追加

```xml
<!-- Sentinel -->
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-sentinel</artifactId>
</dependency>
<dependency>
    <groupId>com.alibaba.csp</groupId>
    <artifactId>sentinel-datasource-nacos</artifactId>
</dependency>

<!-- Nacos -->
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
</dependency>
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableDiscoveryClient` 注解

```java
@SpringBootApplication
@EnableDiscoveryClient
public class SentinelProvider3301 {
    public static void main(String[] args) {
        SpringApplication.run(SentinelProvider3301.class, args);
    }
}
```

- 在bootstrap.yml追加配置

```yml
server:
  port: 3301

spring:
  application:
    name: sentinel-provider-service
  cloud:
    nacos:
      discovery:
        server-addr: localhost:8848 # 注册中心
        group:
        namespace:
      config:
        server-addr: localhost:8848 # 配置中心
        file-extension: yaml # 配置文件格式，支持 yaml、xml、properties、json、text、html
        group:
        namespace:
    sentinel:
      transport:
        dashboard: localhost:8080 # 流控中心
        port: 8719 # 8080端口被占用时以此+1不断尝试
      datasource:
        ds1:
          nacos:
            server-addr: localhost:8848
            namespace: sentinel-dev-namespace
            groupId: sentinel-provider-group
            dataId: sentinel-provider-service
            data-type: json
            rule-type: flow

management:
  endpoints:
    web:
      exposure:
        include: "*" # 暴露/actuator
```

- 编写Controller类

```java
// TODO
```



### 5. 逐一启动各微服务

```bash
# 逐一启动 nacos-server-8848.bat（注册中心）、sentinel-server-8080（流控中心）、sentinel-provider-3301（微服务实例 生产者）
```



### 6. 测试用例

```bash
# TODO
```

