package com.alibaba.cloudy;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@Slf4j
@EnableDiscoveryClient
public class SentinelProvider3301 {

    public static void main(String[] args) {
        SpringApplication.run(SentinelProvider3301.class, args);
        log.info("(づ￣ 3￣)づ(づ￣ 3￣)づ(づ￣ 3￣)づ(づ￣ 3￣)づΨ(￣∀￣)ΨΨ(￣∀￣)ΨΨ(￣∀￣)Ψ");
    }

}
