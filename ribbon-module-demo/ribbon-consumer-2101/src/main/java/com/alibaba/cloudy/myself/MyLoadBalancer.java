package com.alibaba.cloudy.myself;

import org.springframework.cloud.client.ServiceInstance;

import java.util.List;

public interface MyLoadBalancer {

    ServiceInstance selectOne(List<ServiceInstance> services);

}
