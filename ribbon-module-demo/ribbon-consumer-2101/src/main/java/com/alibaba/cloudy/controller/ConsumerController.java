package com.alibaba.cloudy.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.cloudy.entities.Product;
import com.alibaba.cloudy.entities.Results;
import com.alibaba.cloudy.myself.MyLoadBalancer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@RequestMapping("/consumer")
@RestController
@Slf4j
public class ConsumerController {

    private static final String PROVIDER_URL = "http://RIBBON-PROVIDER-SERVICE";

    @Resource
    private RestTemplate restTemplate;

    @Resource
    private DiscoveryClient discoveryClient;

    @Resource
    private MyLoadBalancer myLoadBalancer;

    @GetMapping("/product/get/{id}")
    public Results getOne(@PathVariable("id") Long id) {
        return restTemplate.getForObject(PROVIDER_URL + "/product/get/" + id, Results.class);
    }

    @PostMapping("/product/new")
    public Results newOne(@RequestBody Product product) {
        return restTemplate.postForObject(PROVIDER_URL + "/product/new", product, Results.class);
    }

    @GetMapping(value = "/discovery")
    public Results discovery() {
        List<String> infos = new ArrayList<>();

        List<String> services = discoveryClient.getServices();
        for (String service : services) {
            List<ServiceInstance> instances = discoveryClient.getInstances(service);
            for (ServiceInstance instance : instances) {
                infos.add(instance.getServiceId() + " " + instance.getUri());
            }
        }
        return new Results(200, infos, "SUCCESS");
    }

    @GetMapping("/product/getByMyLb/{id}")
    public Object getByMyLb(@PathVariable("id") Long id) {
        List<ServiceInstance> services = discoveryClient.getInstances("RIBBON-PROVIDER-SERVICE");
        if (CollUtil.isEmpty(services)) {
            return new Results(404, null, "FAIL(There is no provider instance object)");
        }

        ServiceInstance service = myLoadBalancer.selectOne(services);
        return HttpUtil.get(service.getUri() + "/product/get/" + id);
    }

}
