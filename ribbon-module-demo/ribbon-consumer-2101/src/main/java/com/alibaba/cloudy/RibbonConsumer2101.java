package com.alibaba.cloudy;

import com.alibaba.myself.MyRibbonRule;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;

@SpringBootApplication
@Slf4j
@EnableDiscoveryClient
@RibbonClient(name = "RIBBON-PROVIDER-SERVICE", configuration = MyRibbonRule.class)
public class RibbonConsumer2101 {

    public static void main(String[] args) {
        SpringApplication.run(RibbonConsumer2101.class, args);
        log.info("Ψ(￣∀￣)ΨΨ(￣∀￣)ΨΨ(￣∀￣)Ψ(づ￣ 3￣)づ(づ￣ 3￣)づ(づ￣ 3￣)づ(づ￣ 3￣)づ");
    }

}
