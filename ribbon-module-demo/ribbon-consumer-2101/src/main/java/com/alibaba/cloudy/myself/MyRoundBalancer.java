package com.alibaba.cloudy.myself;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class MyRoundBalancer implements MyLoadBalancer {

    private AtomicInteger atomicInteger = new AtomicInteger(0);

    private final int incrementAndGet() {
        for (; ; ) {
            int current = this.atomicInteger.get();
            int next = current + 1;
            if (this.atomicInteger.compareAndSet(current, next))
                return next;
        }
    }

    @Override
    public ServiceInstance selectOne(List<ServiceInstance> services) {
        int index = incrementAndGet() % services.size();
        return services.get(index);
    }
}
