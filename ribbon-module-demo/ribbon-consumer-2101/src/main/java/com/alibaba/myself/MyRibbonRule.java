package com.alibaba.myself;

import com.netflix.loadbalancer.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyRibbonRule {

    @Bean
    public IRule getMyRibbonRule() {
        // 轮询方式
        IRule r0 = new RoundRobinRule();
        // 随机方式
        IRule r1 = new RandomRule();
        // 失败重试方式（轮询查找，如遇服务故障，则多次重试）
        IRule r2 = new RetryRule();
        // 高响应高权重方式
        IRule r3 = new WeightedResponseTimeRule();
        // 最佳可用方式（排除多次降级、熔断、限流的服务）
        IRule r4 = new BestAvailableRule();
        // 可用性过滤方式（排除故障的服务）
        IRule r5 = new AvailabilityFilteringRule();
        // 复合考虑区域性能和可用性后
        IRule r6 = new ZoneAvoidanceRule();
        return r1;
    }

}
