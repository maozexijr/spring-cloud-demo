# Netflix Ribbon



### 1. Ribbon

- 服务端负载均衡

  Nginx是服务端负载均衡，大量来自Consumer的请求被Nginx按需转发到各个Provider，整个请求链路的负载均衡策略由Nginx服务端决定；

- 客户端负载均衡

  Ribbon实现客户端的负载均衡，不同于Nginx服务端负载均衡，Ribbon客户端（Consumer）在获取到所有注册表后自主的决定向哪个服务Provider发起请求，而非由三方组件提供请求转发；

- Ribbon提供7种负载均衡策略

（1）RoundRobinRule // 轮询方式

（2）RandomRule // 随机方式

（3）RetryRule // 失败重试方式（轮询查找，如遇服务故障，则多次重试）

（4）WeightedResponseTimeRule // 高响应高权重方式

（5）BestAvailableRule // 最佳可用方式（排除多次降级、熔断、限流的服务）

（6）AvailabilityFilteringRule // 可用性过滤方式（排除故障的服务）

（7）ZoneAvoidanceRule // 复合考虑区域性能和可用性后

  Ribbon默认负载均衡的方式是 RoundRobinRule 轮询方式；Eureka、Consul 等注册中心客户端内置Ribbon，所以无需显式导入Ribbon包；



### 2. 搭建Consul服务端

- 下载

```bash
# https://www.consul.io/downloads
```

- 安装与启动

```bash
# 双击安装
# 开发模式启动（程序文件所在路径下执行）
$ consul agent -dev
```

- 访问

```bash
# http://localhost:8500
# 免密登录
```



### 3. 搭建Consul客户端

##### （1）搭建2个业务实例 Provider

- 建立 `ribbon-provider-2301` 子Module

- 在子pom中追加

```xml
<!-- Consul -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-consul-discovery</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableDiscoveryClient` 注解

```java
@SpringBootApplication
@EnableDiscoveryClient
public class RibbonProvider2301 {
    public static void main(String[] args) {
        SpringApplication.run(RibbonProvider2301.class, args);
    }
}
```

- 在application.yml追加配置

```yml
server:
  port: 2301

spring:
  application:
    name: ribbon-provider-service
  datasource:
    type: com.alibaba.druid.pool.DruidDataSource
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/cloudy?useUnicode=true&characterEncoding=utf-8&useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=Asia/Shanghai
    username: root
    password: 123456
  cloud:
    consul:
      host: localhost
      port: 8500
      discovery:
        service-name: ${spring.application.name}

mybatis-plus:
  configuration:
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl
  mapper-locations: classpath*:mapper/*.xml
  global-config:
    db-config:
      logic-not-delete-value: 0
      logic-delete-value: 1
```

- 编写Controller类

```java
@RestController
@Slf4j
public class ProviderController {

    @Resource
    private ProductService productService;

    @Value("${server.port}")
    private String serverPort;

    @GetMapping(value = "/product/get/{id}")
    public Results<Product> getOne(@PathVariable("id") Long id) {
        Product product = productService.getById(id);
        if (null == product) {
            return new Results<>(404, null, String.format("FAIL(PORT=%s)", serverPort));
        }
        return new Results<>(200, product, String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @PostMapping(value = "/product/new")
    public Results<Product> newOne(@RequestBody Product product) {
        productService.saveOrUpdate(product);
        return new Results<>(200, product, String.format("SUCCESS(PORT=%s)", serverPort));
    }
}
```

- 同样方法创建 `ribbon-provider-2302`



##### （2）搭建1个业务实例 Consumer

- 建立 `ribbon-consumer-2101` 子Module

- 在子pom中追加

```xml
<!-- Consul -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-consul-discovery</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableDiscoveryClient` 注解

```java
@SpringBootApplication
@EnableDiscoveryClient
public class RibbonConsumer2101 {
    public static void main(String[] args) {
        SpringApplication.run(RibbonConsumer2101.class, args);
    }
}
```

- 在application.yml追加配置

```yml
server:
  port: 2101

spring:
  application:
    name: ribbon-consumer-service
  cloud:
    consul:
      host: localhost
      port: 8500
      discovery:
        service-name: ${spring.application.name}
```

- 添加RestTemplate配置
```java
@Configuration
public class ApplicationContextConfig {

    @Bean
    @LoadBalanced // !important
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}
```

- 编写Controller类

```java
@RequestMapping("/consumer")
@RestController
@Slf4j
public class ConsumerController {

    private static final String PROVIDER_URL = "http://RIBBON-PROVIDER-SERVICE";

    @Resource
    private RestTemplate restTemplate;

    @GetMapping("/product/get/{id}")
    public Results getOne(@PathVariable("id") Long id) {
        return restTemplate.getForObject(PROVIDER_URL + "/product/get/" + id, Results.class);
    }
}
```

### 4. 逐一启动各微服务

```bash
# 逐一启动 consul-server-8500.bat（注册中心）、ribbon-provider-2301（微服务实例 生产者）、ribbon-provider-2302（微服务实例 生产者）、ribbon-consumer-2101（微服务实例 消费者）
```



### 5. 测试用例（默认方式）

```bash
# 测试服务间的通信和负载均衡
$ curl http://localhost:2101/consumer/product/get/1
> {"code":200,"data":{"id":1,"name":"book","price":30.0,"count":2},"message":"SUCCESS(PORT=2301)"}
$ curl http://localhost:2101/consumer/product/get/1
> {"code":200,"data":{"id":1,"name":"book","price":30.0,"count":2},"message":"SUCCESS(PORT=2302)"}
```



### 6. Ribbon的其他负载均衡方式

- 在Consumer端 @ComponentScan扫描不到的位置新建类 `MyRibbonRule`

```java
package com.alibaba.myself;

import com.netflix.loadbalancer.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyRibbonRule {

    @Bean
    public IRule getMyRibbonRule() {
        // 轮询方式
        IRule r0 = new RoundRobinRule();
        // 随机方式
        IRule r1 = new RandomRule();
        // 失败重试方式（轮询查找，如遇服务故障，则多次重试）
        IRule r2 = new RetryRule();
        // 高响应高权重方式
        IRule r3 = new WeightedResponseTimeRule();
        // 最佳可用方式（排除多次降级、熔断、限流的服务）
        IRule r4 = new BestAvailableRule();
        // 可用性过滤方式（排除故障的服务）
        IRule r5 = new AvailabilityFilteringRule();
        // 复合考虑区域性能和可用性后
        IRule r6 = new ZoneAvoidanceRule();
        return r1;
    }
}
```

- 在SpringBoot启动类追加 `@RibbonClient` 注解

```java
@SpringBootApplication
@EnableDiscoveryClient
@RibbonClient(name = "RIBBON-PROVIDER-SERVICE", configuration = MyRibbonRule.class)
public class RibbonConsumer2101 {
    public static void main(String[] args) {
        SpringApplication.run(RibbonConsumer2101.class, args);
    }
}
```

- 测试随机方式的负载均衡

```bash
# 注意看端口
$ curl http://localhost:2101/consumer/product/get/1
> {"code":200,"data":{"id":1,"name":"Thinking in Java"},"message":"SUCCESS(PORT=2301)"}

$ curl http://localhost:2101/consumer/product/get/1
> {"code":200,"data":{"id":1,"name":"Thinking in Java"},"message":"SUCCESS(PORT=2301)"}

$ curl http://localhost:2101/consumer/product/get/1
> {"code":200,"data":{"id":1,"name":"Thinking in Java"},"message":"SUCCESS(PORT=2302)"}
```



### 7. 手写轮询方式的负载均衡

- 在Consumer端手写均衡负载的接口和实现类

```java
public interface MyLoadBalancer {
    ServiceInstance selectOne(List<ServiceInstance> services);
}
```

```java
@Component // !important
public class MyRoundBalancer implements MyLoadBalancer {

    private AtomicInteger atomicInteger = new AtomicInteger(0);
    private final int incrementAndGet() {
        for (; ; ) {
            int current = this.atomicInteger.get();
            int next = current + 1;
            if (this.atomicInteger.compareAndSet(current, next))
                return next;
        }
    }

    @Override
    public ServiceInstance selectOne(List<ServiceInstance> services) {
        int index = incrementAndGet() % services.size();
        return services.get(index);
    }
}
```

- 修改Controller调用

```java
@Resource
private DiscoveryClient discoveryClient;

@Resource
private MyLoadBalancer myLoadBalancer;

@GetMapping("/consumer/product/getByMyLb/{id}")
public Object getByMyLb(@PathVariable("id") Long id) {
  List<ServiceInstance> services = discoveryClient.getInstances("RIBBON-PROVIDER-SERVICE");
  if (CollUtil.isEmpty(services)) {
    return new Results(404, null, "FAIL(There is no provider instance object)");
  }

  ServiceInstance service = myLoadBalancer.selectOne(services);
  return HttpUtil.get(service.getUri() + "/product/get/" + id);
}
```

- 测试手写轮询的负载均衡

```bash
# 注意看端口
$ curl http://localhost:2101/consumer/product/getByMyLb/1
> {"code":200,"data":{"id":1,"name":"Thinking in Java"},"message":"SUCCESS(PORT=2301)"}

$ curl http://localhost:2101/consumer/product/getByMyLb/1
> {"code":200,"data":{"id":1,"name":"Thinking in Java"},"message":"SUCCESS(PORT=2302)"}

$ curl http://localhost:2101/consumer/product/getByMyLb/1
> {"code":200,"data":{"id":1,"name":"Thinking in Java"},"message":"SUCCESS(PORT=2301)"}

$ curl http://localhost:2101/consumer/product/getByMyLb/1
> {"code":200,"data":{"id":1,"name":"Thinking in Java"},"message":"SUCCESS(PORT=2302)"}
```
