# Spring Cloud Bus



### 1. Spring Cloud Bus

  Spring Cloud Bus解决了Spring Cloud Config只能点对点的更新通知（curl -X POST /actuator/refresh），实现了“一点触发处处广播”的配置更新，使用更高效、便利。

  消息总线：使用消息中间件作为更新通知的中枢，每个微服务都订阅同一主题（SpringCloudBus）,一旦消息变更（存在配置更新），（几乎）立刻响应到各订阅的微服务上，从而实现了“一点触发处处广播”的配置文件更新。

- Spring Cloud Bus的两种广播方式：

（1）运维在修改配置中心的配置文件后，主动推送更新到某一个微服务，Spring Cloud Bus主动拉取该服务的更新内容，并广播更新到其他微服务，进而完成所有微服务的配置更新；此类方式增加了服务的额外负担，耦合了非业务功能，不推荐此类实践；

（2）运维在修改配置中心的配置文件后，主动推送更新到配置中心（Config Center），由配置中心广播更新内容，完成配置文件的全局的动态刷新；（推荐）



### 2. 安装消息中间件

  Spring Cloud Bus的使用是基于消息中间件的，目前仅支持RabbitMQ和Kafka（不支持RocketMQ和ActiveMQ）

**环境准备**

- Erlang安装与配置

（1）下载并以默认方式安装

```bash
# https://erlang.org/download/otp_win64_24.0.exe
```

（2）配置环境变量

```bash
# ERLANG_HOME=C:\Program Files\erl-24.0
# Path=%ERLANG_HOME%\bin;
```

（3）校验是否安装成功

```bash
$ erl
> Eshell V12.0  (abort with ^G)
```


- RabbitMQ安装与配置

（1）下载并以默认方式安装

```bash
# https://www.rabbitmq.com/install-windows.html
# 注意安装路径和主机名不要有中文
```

（2）安装相关插件

```bash
# 在安装目录的sbin下以管理员权限执行
# 菜单栏找到 “RabbitMQ Command Prompt(sbin dir)”
$ rabbitmq-plugins enable rabbitmq_management
```

（3）启动服务

```bash
# 在安装目录的sbin下以管理员权限执行 rabbitmq-server.bat
```

（4）访问主页

```bash
# 访问 http://localhost:15672/
# 默认账密guest/guest
```



### 3. 搭建微服务、注册中心、配置中心、配置仓库

搭建步骤类比 config-module-demo ，详情查看 [《微服务配置中心各组件搭建与测试》](../config-module-demo/README.md)



### 4. 适配Spring Cloud Bus

##### （1）修改2个业务实例 Provider

- 在 `bus-provider-2301` 的pom中追加

```xml
<!-- Bus + MQ -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-bus-amqp</artifactId>
</dependency>

<!-- Config -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-config-server</artifactId>
</dependency>

<!-- Consul -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-consul-discovery</artifactId>
</dependency>
```

- 在**bootstrap.yml**追加配置

```yml
server:
  port: 2301

spring:
  application:
    name: bus-provider-service
  cloud:
    consul:
      host: localhost
      port: 8500
      discovery:
        service-name: ${spring.application.name}
    config:
      label: master # {label} 分支名，如 master
      name: config # {application} 配置文件前缀，即应用名称
      profile: dev # {profile} 配置文件后缀，即环境类型，如 dev/test/prod
      # /{label}/{application}-{profile}.yml 如 /master/config-dev.yml
      uri: http://localhost:2201 # 配置中心
  rabbitmq:
    host: localhost
    port: 5672 # 主页端口15672，注意区分
    username: guest
    password: guest

profile:
  version: v1.0 # bootstrap.yml的优先级高于application.yml

management:
  endpoints:
    web:
      exposure:
        include: "bus-refresh" # 暴露/actuator，以供刷新
        # curl -X POST http://localhost:2301/actuator/bus-refresh
```

- 同样方法创建 `bus-provider-2302`



##### （2）修改配置中心

- 在 `bus-center-2201` 的pom中追加

```xml
<!-- Bus + MQ -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-bus-amqp</artifactId>
</dependency>

<!-- Config -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-config-server</artifactId>
</dependency>

<!-- Consul -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-consul-discovery</artifactId>
</dependency>
```

- 在**application.yml**追加配置

```yml
server:
  port: 2201

spring:
  application:
    name: bus-center-service
  cloud:
    consul:
      host: localhost
      port: 8500
      discovery:
        service-name: ${spring.application.name}
    config:
      server:
        git:
          uri: https://gitee.com/maozexijr/cloudy.git # 配置中心
          search-paths:
            - bus-module-demo/bus-center-2201/profiles # 配置文件所在路径
      label: master # 分支名
  rabbitmq:
    host: localhost
    port: 5672 # 主页端口15672，注意区分
    username: guest
    password: guest

management:
  endpoints:
    web:
      exposure:
        include: "bus-refresh" # 暴露/actuator，以供刷新
        # curl -X POST http://localhost:2201/actuator/bus-refresh
```



### 5. 逐一启动各微服务

```bash
# 逐一启动 rabbitmq-server.bat（消息中间件）、consul-server-8500.bat（注册中心）、bus-center-2201（配置中心）、bus-provider-2301（微服务实例）、bus-provider-2302（微服务实例）
```



### 6. 测试用例

```bash
# 测试配置中心
$ curl http://localhost:2201/master/config-dev.yml
> profile:
>   name: config-dev.yml
>   version: v1.0

# 测试Provider
$ curl http://localhost:2301/profile/version
> {"code":200,"data":"v1.0","message":"SUCCESS(PORT=2301)"}
$ curl http://localhost:2302/profile/version
> {"code":200,"data":"v1.0","message":"SUCCESS(PORT=2302)"}

# 修改码云仓库config-dev.yml的version为v2.0后
$ curl http://localhost:2201/master/config-dev.yml
> profile:
>   name: config-dev.yml
>   version: v2.0
# 发现注册中心及时更新了配置

# 执行
$ curl http://localhost:2301/profile/version
> {"code":200,"data":"v1.0","message":"SUCCESS(PORT=2301)"}
$ curl http://localhost:2302/profile/version
> {"code":200,"data":"v1.0","message":"SUCCESS(PORT=2302)"}
# 发现各Provider仍未更新配置

# 手动刷新
$ curl -X POST http://localhost:2201/actuator/bus-refresh
>
# 后再次
$ curl http://localhost:2301/profile/version
> {"code":200,"data":"v2.0","message":"SUCCESS(PORT=2301)"}
$ curl http://localhost:2302/profile/version
> {"code":200,"data":"v2.0","message":"SUCCESS(PORT=2302)"}
# 发现各Provider的配置都已更新

# 修改码云仓库config-dev.yml的version为v3.0
$ curl -X POST http://localhost:2201/actuator/bus-refresh/bus-provider-service:2301
# 后再次
$ curl http://localhost:2301/profile/version
> {"code":200,"data":"v3.0","message":"SUCCESS(PORT=2301)"}
# 发现 `config-provider-2301`的配置已更新
$ curl http://localhost:2302/profile/version
> {"code":200,"data":"v2.0","message":"SUCCESS(PORT=2302)"}
# 但是！`config-provider-2302`的配置仍未更新
```



### 7. 全局通知或定点通知

运维修改 `配置仓库` 后需手动通知 `配置中心`，各微服务随之更新配置

```bash
$ curl -X POST http://localhost:2201/actuator/bus-refresh
```

如需定点通知，执行如下命令

```bash
$ curl -X POST http://localhost:2201/actuator/bus-refresh/bus-provider-service:2301
# 在全局通知命令的尾部增加 要定点通知的服务名和端口号
```