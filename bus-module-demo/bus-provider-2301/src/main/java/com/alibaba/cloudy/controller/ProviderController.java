package com.alibaba.cloudy.controller;

import com.alibaba.cloudy.entities.Results;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RefreshScope // ! important
public class ProviderController {

    @Value("${server.port}")
    private String serverPort;

    @Value("${profile.name}")
    private String profileName;

    @Value("${profile.version}")
    private String profileVersion;

    @GetMapping(value = "/profile/name")
    public Results<String> getName() {
        return new Results<>(200, profileName, String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @GetMapping(value = "/profile/version")
    public Results<String> getVersion() {
        return new Results<>(200, profileVersion, String.format("SUCCESS(PORT=%s)", serverPort));
    }
}
