java -jar zipkin-server-2.12.9-exec.jar


#                                               ┏━━━━━━━━━━━━━━━┓
#                                               ┃  span id = c  ┃
#                                               ┃ parent id = b ┃
# ┏━━━━━━━━━━━━━━━┓      ┏━━━━━━━━━━━━━━━┓  ->  ┃  trace id = α ┃
# ┃  span id = a  ┃  ->  ┃  span id = b  ┃  <-  ┗━━━━━━━━━━━━━━━┛
# ┃ parent id =   ┃      ┃ parent id = a ┃
# ┃  trace id = α ┃  <-  ┃  trace id = α ┃      ┏━━━━━━━━━━━━━━━┓
# ┗━━━━━━━━━━━━━━━┛      ┗━━━━━━━━━━━━━━━┛  ->  ┃  span id = d  ┃
#                                           <-  ┃ parent id = b ┃
#                                               ┃  trace id = α ┃
#                                               ┗━━━━━━━━━━━━━━━┛