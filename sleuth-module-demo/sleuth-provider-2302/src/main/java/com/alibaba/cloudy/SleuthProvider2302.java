package com.alibaba.cloudy;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


@SpringBootApplication
@Slf4j
@EnableDiscoveryClient
public class SleuthProvider2302 {

    public static void main(String[] args) {
        SpringApplication.run(SleuthProvider2302.class, args);
        log.info("Ψ(￣∀￣)ΨΨ(￣∀￣)ΨΨ(￣∀￣)Ψ(づ￣ 3￣)づ(づ￣ 3￣)づ(づ￣ 3￣)づ(づ￣ 3￣)づ");
    }

}