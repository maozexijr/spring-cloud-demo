package com.alibaba.cloudy.controller;

import com.alibaba.cloudy.entities.Results;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@RequestMapping(value = "/middler")
@RestController
@Slf4j
public class MiddlerController {

    @Value("${server.port}")
    private String serverPort;

    private static final String PROVIDER_URL = "http://SLEUTH-PROVIDER-SERVICE";

    @Resource
    private RestTemplate restTemplate;

    @GetMapping(value = "/trace")
    public Results trace() {
        Results<String> results = restTemplate.getForObject(PROVIDER_URL + "/provider/trace", Results.class);
        String data = String.format("%s -> %s", serverPort, results.getData());
        return new Results<>(200, data, String.format("SUCCESS(PORT=%s)", serverPort));
    }

}
