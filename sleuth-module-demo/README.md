# Spring Cloud Sleuth



### 1. Spring Cloud Sleuth

  Spring Cloud Sleuth为分布式系统调用链路的监控提供支持，其实现依赖于Zipkin，具体讲 Sleuth负责监控，Zipkin负责图形页面展示。

- Sleuth监控中心重要概念：

（1）trace id：一个完整的调用和响应链路共用一个trace id
（2）span id：请求的发出方视为一个span
（3）parent id：请求的接收方用以记录请求来源的标志

- Sleuth监控中心业务流程：

```bash
#                                               ┏━━━━━━━━━━━━━━━━┓
#                                               ┃  span id = c  ┃
#                                               ┃ parent id = b ┃
# ┏━━━━━━━━━━━━━━━━┓       ┏━━━━━━━━━━━━━━━┓  ->   ┃  trace id = 1 ┃
# ┃  span id = a  ┃  ->  ┃  span id = b  ┃  <-   ┗━━━━━━━━━━━━━━━┛
# ┃ parent id =   ┃      ┃ parent id = a ┃
# ┃  trace id = 1 ┃  <-  ┃  trace id = 1 ┃       ┏━━━━━━━━━━━━━━━┓
# ┗━━━━━━━━━━━━━━━━┛       ┗━━━━━━━━━━━━━━━┛  ->   ┃  span id = d  ┃
#                                           <-  ┃ parent id = b ┃
#                                               ┃  trace id = 1 ┃
#                                               ┗━━━━━━━━━━━━━━━━┛ 
```



### 2. 启用Sleuth监控中心

- 下载

```bash
# https://search.maven.org/remote_content?g=io.zipkin.java&a=zipkin-server&v=LATEST&c=exec
```

- 启动

```bash
# 程序文件所在路径下执行
$ java -jar zipkin-server-2.12.9-exec.jar
```

- 访问

```bash
# http://localhost:9411/zipkin
# 免密登录
```


### 3. 搭建Consul服务端

- 下载

```bash
# https://www.consul.io/downloads
```

- 安装与启动

```bash
# 双击安装
# 开发模式启动（程序文件所在路径下执行）
$ consul agent -dev
```

- 访问

```bash
# http://localhost:8500
# 免密登录
```



### 4. 搭建Consul客户端

##### （1）搭建2个业务实例 Provider

- 建立 `sleuth-provider-2301` 子Module

- 在子pom中追加

```xml
<!-- Sleuth + Zipkin -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-zipkin</artifactId>
</dependency>

<!-- Consul -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-consul-discovery</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableDiscoveryClient` 注解

```java
@SpringBootApplication
@EnableDiscoveryClient
public class SleuthProvider2301 {
    public static void main(String[] args) {
        SpringApplication.run(SleuthProvider2301.class, args);
    }
}
```

- 在application.yml追加配置

```yml
server:
  port: 2301

spring:
  application:
    name: sleuth-provider-service
  cloud:
    consul:
      host: localhost
      port: 8500
      discovery:
        service-name: ${spring.application.name}
  zipkin:
    base-url: http://localhost:9411 # 监控中心
  sleuth:
    sampler:
      probability: 1 # 采样率 0-1之间

#                                               ┏━━━━━━━━━━━━━━━┓
#                                               ┃  span id = c  ┃
#                                               ┃ parent id = b ┃
# ┏━━━━━━━━━━━━━━━┓         ┏━━━━━━━━━━━━━━━┓  ->  ┃  trace id = 1 ┃
# ┃  span id = a  ┃  ->   ┃  span id = b  ┃  <-  ┗━━━━━━━━━━━━━━━┛
# ┃ parent id =   ┃       ┃ parent id = a ┃
# ┃  trace id = 1 ┃  <-   ┃  trace id = 1 ┃      ┏━━━━━━━━━━━━━━━┓
# ┗━━━━━━━━━━━━━━━┛         ┗━━━━━━━━━━━━━━━┛  ->  ┃  span id = d  ┃
#                                           <-  ┃ parent id = b ┃
#                                               ┃  trace id = 1 ┃
#                                               ┗━━━━━━━━━━━━━━━┛
```

- 编写Controller类

```java
@RequestMapping(value = "/provider")
@RestController
@Slf4j
public class ProviderController {

    @Value("${server.port}")
    private String serverPort;

    @GetMapping(value = "/trace")
    public Results trace() {
        return new Results<>(200, serverPort, String.format("SUCCESS(PORT=%s)", serverPort));
    }

}
```

- 同样方法创建 `sleuth-provider-2302`



##### （2）搭建1个业务实例 Middler

- 建立 `sleuth-middler-2201` 子Module

- 在子pom中追加

```xml
<!-- Sleuth + Zipkin -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-zipkin</artifactId>
</dependency>

<!-- Consul -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-consul-discovery</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableDiscoveryClient` 注解

```java
@SpringBootApplication
@EnableDiscoveryClient
public class SleuthMiddler2201 {
    public static void main(String[] args) {
        SpringApplication.run(SleuthMiddler2201.class, args);
    }
}
```

- 在application.yml追加配置

```yml
server:
  port: 2201

spring:
  application:
    name: sleuth-middler-service
  cloud:
    consul:
      host: localhost
      port: 8500
      discovery:
        service-name: ${spring.application.name}
  zipkin:
    base-url: http://localhost:9411 # 监控中心
  sleuth:
    sampler:
      probability: 1 # 采样率 0-1之间


#                                               ┏━━━━━━━━━━━━━━━┓
#                                               ┃  span id = c  ┃
#                                               ┃ parent id = b ┃
# ┏━━━━━━━━━━━━━━━┓         ┏━━━━━━━━━━━━━━━┓  ->  ┃  trace id = 1 ┃
# ┃  span id = a  ┃  ->  ┃  span id = b  ┃  <-  ┗━━━━━━━━━━━━━━━┛
# ┃ parent id =   ┃      ┃ parent id = a ┃
# ┃  trace id = 1 ┃  <-  ┃  trace id = 1 ┃      ┏━━━━━━━━━━━━━━━┓
# ┗━━━━━━━━━━━━━━━┛         ┗━━━━━━━━━━━━━━━┛  ->  ┃  span id = d  ┃
#                                           <-  ┃ parent id = b ┃
#                                               ┃  trace id = 1 ┃
#                                               ┗━━━━━━━━━━━━━━━┛
```

- 添加RestTemplate配置
```java
@Configuration
public class ApplicationContextConfig {

    @Bean
    @LoadBalanced // !important
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}
```

- 编写Controller类

```java
@RequestMapping(value = "/middler")
@RestController
@Slf4j
public class MiddlerController {

    @Value("${server.port}")
    private String serverPort;

    private static final String PROVIDER_URL = "http://SLEUTH-PROVIDER-SERVICE";

    @Resource
    private RestTemplate restTemplate;

    @GetMapping(value = "/trace")
    public Results trace() {
        Results<String> results = restTemplate.getForObject(PROVIDER_URL + "/provider/trace", Results.class);
        String data = String.format("%s -> %s", serverPort, results.getData());
        return new Results<>(200, data, String.format("SUCCESS(PORT=%s)", serverPort));
    }

}
```



##### （3）搭建1个业务实例 Consumer

- 建立 `sleuth-consumer-2101` 子Module

- 在子pom中追加

```xml
<!-- Sleuth + Zipkin -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-zipkin</artifactId>
</dependency>

<!-- Consul -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-consul-discovery</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableDiscoveryClient` 注解

```java
@SpringBootApplication
@EnableDiscoveryClient
public class SleuthConsumer2101 {
    public static void main(String[] args) {
        SpringApplication.run(SleuthConsumer2101.class, args);
    }
}
```

- 在application.yml追加配置

```yml
server:
  port: 2101

spring:
  application:
    name: sleuth-consumer-service
  cloud:
    consul:
      host: localhost
      port: 8500
      discovery:
        service-name: ${spring.application.name}
  zipkin:
    base-url: http://localhost:9411 # 监控中心
  sleuth:
    sampler:
      probability: 1 # 采样率 0-1之间


#                                               ┏━━━━━━━━━━━━━━━┓
#                                               ┃  span id = c  ┃
#                                               ┃ parent id = b ┃
# ┏━━━━━━━━━━━━━━━┓         ┏━━━━━━━━━━━━━━━┓  ->  ┃  trace id = α ┃
# ┃  span id = a  ┃  ->  ┃  span id = b  ┃  <-  ┗━━━━━━━━━━━━━━━┛
# ┃ parent id =   ┃      ┃ parent id = a ┃
# ┃  trace id = 1 ┃  <-  ┃  trace id = 1 ┃      ┏━━━━━━━━━━━━━━━┓
# ┗━━━━━━━━━━━━━━━┛         ┗━━━━━━━━━━━━━━━┛  ->  ┃  span id = d  ┃
#                                           <-  ┃ parent id = b ┃
#                                               ┃  trace id = 1 ┃
#                                               ┗━━━━━━━━━━━━━━━┛
```

- 添加RestTemplate配置
```java
@Configuration
public class ApplicationContextConfig {

    @Bean
    @LoadBalanced // !important
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}
```

- 编写Controller类

```java
@RequestMapping(value = "/consumer")
@RestController
@Slf4j
public class ConsumerController {

    @Value("${server.port}")
    private String serverPort;

    private static final String MIDDLER_URL = "http://SLEUTH-MIDDLER-SERVICE";

    @Resource
    private RestTemplate restTemplate;

    @GetMapping(value = "/trace")
    public Results<String> trace() {
        Results<String> results = restTemplate.getForObject(MIDDLER_URL + "/middler/trace", Results.class);
        String data = String.format("%s -> %s", serverPort, results.getData());
        return new Results<>(200, data, String.format("SUCCESS(PORT=%s)", serverPort));
    }

}
```



### 5. 逐一启动各微服务

```bash
# 逐一启动 sleuth-9411-zipkin.bat（监控中心）、consul-server-8500.bat（注册中心）、sleuth-provider-2301（微服务实例 生产者）、sleuth-provider-2302（微服务实例 生产者）、sleuth-middler-2201（微服务实例 生产者兼消费者）、sleuth-consumer-2101（微服务实例 消费者）
```



### 6. 测试用例

```bash
$ curl http://localhost:2101/consumer/trace
> {"code":200,"data":"2101 -> 2201 -> 2301","message":"SUCCESS(PORT=2101)"}
$ curl http://localhost:2101/consumer/trace
> {"code":200,"data":"2101 -> 2201 -> 2302","message":"SUCCESS(PORT=2101)"}

# 查看监控中心 http://localhost:9411/zipkin
# Service Name                        Span Name
# sleuth-consumer-service             get /consumer/trace
#   ┗━ sleuth-middler-service         get /middler/trace
#       ┗━ sleuth-provider-service    get /provider/trace
```

- 链路示例

```json
[
  [
    {
      "traceId": "c7456b2b65508fd3",
      "parentId": "c7456b2b65508fd3",
      "id": "a29de1f72980a949",
      "kind": "CLIENT",
      "name": "get",
       "duration": 49088,
      "localEndpoint": {
        "serviceName": "sleuth-consumer-service",
        "ipv4": "192.168.1.1"
      },
      "tags": {
        "http.method": "GET",
        "http.path": "/middler/trace"
      }
    },
    {
      "traceId": "c7456b2b65508fd3",
      "id": "c7456b2b65508fd3",
      "kind": "SERVER",
      "name": "get /consumer/trace",
      "duration": 54737,
      "localEndpoint": {
        "serviceName": "sleuth-consumer-service",
        "ipv4": "192.168.1.1"
      },
      "remoteEndpoint": {
        "ipv6": "::1",
        "port": 56149
      },
      "tags": {
        "http.method": "GET",
        "http.path": "/consumer/trace",
        "mvc.controller.class": "ConsumerController",
        "mvc.controller.method": "trace"
      }
    },
    {
      "traceId": "c7456b2b65508fd3",
      "parentId": "a29de1f72980a949",
      "id": "fd378eecb4af30c2",
      "kind": "CLIENT",
      "name": "get",
      "duration": 36741,
      "localEndpoint": {
        "serviceName": "sleuth-middler-service",
        "ipv4": "192.168.1.1"
      },
      "tags": {
        "http.method": "GET",
        "http.path": "/provider/trace"
      }
    },
    {
      "traceId": "c7456b2b65508fd3",
      "parentId": "c7456b2b65508fd3",
      "id": "a29de1f72980a949",
      "kind": "SERVER",
      "name": "get /middler/trace",
      "duration": 42576,
      "localEndpoint": {
        "serviceName": "sleuth-middler-service",
        "ipv4": "192.168.1.1"
      },
      "remoteEndpoint": {
        "ipv4": "192.168.1.2",
        "port": 52813
      },
      "tags": {
        "http.method": "GET",
        "http.path": "/middler/trace",
        "mvc.controller.class": "MiddlerController",
        "mvc.controller.method": "trace"
      },
      "shared": true
    },
    {
      "traceId": "c7456b2b65508fd3",
      "parentId": "a29de1f72980a949",
      "id": "fd378eecb4af30c2",
      "kind": "SERVER",
      "name": "get /provider/trace",
      "duration": 21824,
      "localEndpoint": {
        "serviceName": "sleuth-provider-service",
        "ipv4": "192.168.1.1"
      },
      "remoteEndpoint": {
        "ipv4": "192.168.1.2",
        "port": 56150
      },
      "tags": {
        "http.method": "GET",
        "http.path": "/provider/trace",
        "mvc.controller.class": "ProviderController",
        "mvc.controller.method": "trace"
      },
      "shared": true
    }
  ],
  [
    {
      "traceId": "cda1478547a07701",
      "parentId": "11eb960640da2639",
      "id": "c0e844c0a5003f8e",
      "kind": "CLIENT",
      "name": "get",
      "duration": 216628,
      "localEndpoint": {
        "serviceName": "sleuth-middler-service",
        "ipv4": "192.168.1.1"
      },
      "tags": {
        "http.method": "GET",
        "http.path": "/provider/trace"
      }
    },
    {
      "traceId": "cda1478547a07701",
      "parentId": "cda1478547a07701",
      "id": "11eb960640da2639",
      "kind": "SERVER",
      "name": "get /middler/trace",
      "duration": 262542,
      "localEndpoint": {
        "serviceName": "sleuth-middler-service",
        "ipv4": "192.168.1.1"
      },
      "remoteEndpoint": {
        "ipv4": "192.168.1.2",
        "port": 52813
      },
      "tags": {
        "http.method": "GET",
        "http.path": "/middler/trace",
        "mvc.controller.class": "MiddlerController",
        "mvc.controller.method": "trace"
      },
      "shared": true
    },
    {
      "traceId": "cda1478547a07701",
      "parentId": "cda1478547a07701",
      "id": "11eb960640da2639",
      "kind": "CLIENT",
      "name": "get",
      "duration": 531122,
      "localEndpoint": {
        "serviceName": "sleuth-consumer-service",
        "ipv4": "192.168.1.1"
      },
      "tags": {
        "http.method": "GET",
        "http.path": "/middler/trace"
      }
    },
    {
      "traceId": "cda1478547a07701",
      "id": "cda1478547a07701",
      "kind": "SERVER",
      "name": "get /consumer/trace",
      "duration": 619020,
      "localEndpoint": {
        "serviceName": "sleuth-consumer-service",
        "ipv4": "192.168.1.1"
      },
      "remoteEndpoint": {
        "ipv6": "::1",
        "port": 52812
      },
      "tags": {
        "http.method": "GET",
        "http.path": "/consumer/trace",
        "mvc.controller.class": "ConsumerController",
        "mvc.controller.method": "trace"
      }
    },
    {
      "traceId": "cda1478547a07701",
      "parentId": "11eb960640da2639",
      "id": "c0e844c0a5003f8e",
      "kind": "SERVER",
      "name": "get /provider/trace",
      "duration": 10429,
      "localEndpoint": {
        "serviceName": "sleuth-provider-service",
        "ipv4": "192.168.1.1"
      },
      "remoteEndpoint": {
        "ipv4": "192.168.1.2",
        "port": 52814
      },
      "tags": {
        "http.method": "GET",
        "http.path": "/provider/trace",
        "mvc.controller.class": "ProviderController",
        "mvc.controller.method": "trace"
      },
      "shared": true
    }
  ]
]
```