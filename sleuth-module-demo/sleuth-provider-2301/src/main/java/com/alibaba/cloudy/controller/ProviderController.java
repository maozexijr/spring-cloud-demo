package com.alibaba.cloudy.controller;

import com.alibaba.cloudy.entities.Results;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(value = "/provider")
@RestController
@Slf4j
public class ProviderController {

    @Value("${server.port}")
    private String serverPort;

    @GetMapping(value = "/trace")
    public Results trace() {
        //
        return new Results<>(200, serverPort, String.format("SUCCESS(PORT=%s)", serverPort));
    }

}
