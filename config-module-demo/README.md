# Spring Cloud Config



### 1. Spring Cloud Config

  Spring Cloud Config集中的管理分布式系统各服务的配置文件，动态刷新配置修改，而无需服务重启。

```bash
# bootstrap.yml是系统配置（全局的），application.yml是应用配置（局部的），优先级上 bootstrap.yml比application.yml更高
```



### 2. 搭建Consul服务端

- 下载

```bash
# https://www.consul.io/downloads
```

- 安装与启动

```bash
# 双击安装
# 开发模式启动（程序文件所在路径下执行）
$ consul agent -dev
```

- 访问

```bash
# http://localhost:8500
# 免密登录
```



### 3. 搭建Consul客户端

##### （1）搭建2个业务实例 Provider

- 建立 `config-provider-2301` 子Module

- 在子pom中追加

```xml
<!-- Config -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-config-client</artifactId>
</dependency>

<!-- Consul -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-consul-discovery</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableDiscoveryClient` 注解

```java
@SpringBootApplication
@EnableDiscoveryClient
public class ConfigProvider2301 {
    public static void main(String[] args) {
        SpringApplication.run(ConfigProvider2301.class, args);
    }
}
```

- 在**bootstrap.yml**追加配置

```yml
server:
  port: 2301

spring:
  application:
    name: config-provider-service
  cloud:
    consul:
      host: localhost
      port: 8500
      discovery:
        service-name: ${spring.application.name}
    config:
      label: master # {label} 分支名，如 master
      name: config # {application} 配置文件前缀，即应用名称
      profile: dev # {profile} 配置文件后缀，即环境类型，如 dev/test/prod
      # /{label}/{application}-{profile}.yml 如 /master/config-dev.yml
      uri: http://localhost:2201 # 配置中心

profile:
  version: v2.0

management:
  endpoints:
    web:
      exposure:
        include: "*" # 暴露 /actuator，以供刷新
```

- 编写Controller层，注意追加`@RefreshScope`注解

```java
@RestController
@Slf4j
@RefreshScope // ! important
public class ProviderController {

    @Value("${server.port}")
    private String serverPort;

    @Value("${profile.name}")
    private String profileName;

    @Value("${profile.version}")
    private String profileVersion;
    
    @GetMapping(value = "/profile/name")
    public Results<String> getName() {
        return new Results<>(200, profileName, String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @GetMapping(value = "/profile/version")
    public Results<String> getVersion() {
        return new Results<>(200, profileVersion, String.format("SUCCESS(PORT=%s)", serverPort));
    }
}
```

- 同样方法创建 `config-provider-2302`



##### （2）搭建配置中心

- 在`GitHub`或 `码云`上创建配置文件的共享库（配置仓库）

```
# 本例使用的配置文件共享库
# https://gitee.com/maozexijr/cloudy/blob/master/config-module-demo/config-center-2201/profiles/

# 配置文件的访问路径和命名格式存在多种，如下
/{application}/{profile}[/{label}]
/{application}-{profile}.yml
/{label}/{application}-{profile}.yml
/{application}-{profile}.properties
/{label}/{application}-{profile}.properties

# 推荐 /{label}/{application}-{profile}.yml 格式
# 即 /分支名/应用名-环境名.yml，如 /master/config-dev.yml
```

- 建立 `config-center-2201` 子Module

- 在子pom中追加

```xml
<!-- Config -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-config-server</artifactId>
</dependency>

<!-- Consul -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-consul-discovery</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableDiscoveryClient` 和`@EnableConfigServer`注解

```java
@SpringBootApplication
@EnableDiscoveryClient
@EnableConfigServer
public class ConfigCenter2201 {
    public static void main(String[] args) {
        SpringApplication.run(ConfigCenter2201.class, args);
    }
}
```

- 在**application.yml**追加配置

```yml
server:
  port: 2201

spring:
  application:
    name: config-center-service
  cloud:
    consul:
      host: localhost
      port: 8500
      discovery:
        service-name: ${spring.application.name}
    config:
      server:
        git:
          uri: https://gitee.com/maozexijr/cloudy.git # 配置中心（提前创建好）
          search-paths:
            - config-module-demo/config-center-2201/profiles # 配置文件所在路径
      label: master # 分支名

management:
  endpoints:
    web:
      exposure:
        include: "*" # 暴露 /actuator，以供刷新
```



### 4. 逐一启动各微服务

```bash
# 逐一启动 consul-server-8500.bat（注册中心）、config-center-2201（配置中心）、config-provider-2301（微服务实例）、config-provider-2302（微服务实例）
```



### 5. 测试配置更新

```bash
# 测试配置中心
$ curl http://localhost:2201/master/config-dev.yml
> profile:
>    name: config-dev.yml
>    version: v1.0

# 测试Provider
$ curl http://localhost:2301/profile/version
> {"code":200,"data":"v1.0","message":"SUCCESS(PORT=2301)"}
$ curl http://localhost:2302/profile/name
> {"code":200,"data":"config-dev.yml","message":"SUCCESS(PORT=2302)"}

# 修改码云仓库config-dev.yml的version为v2.0
$ curl http://localhost:2201/master/config-dev.yml
> profile:
>    name: config-dev.yml
>    version: v2.0
# 发现注册中心及时更新了配置

# 执行
$ curl http://localhost:2301/profile/version
> {"code":200,"data":"v1.0","message":"SUCCESS(PORT=2301)"}
$ curl http://localhost:2302/profile/version
> {"code":200,"data":"v1.0","message":"SUCCESS(PORT=2302)"}
# 发现各Provider的配置仍未更新

# 手动刷新
$ curl -X POST http://localhost:2301/actuator/refresh
> []
# 后再次
$ curl http://localhost:2301/profile/version
> {"code":200,"data":"v2.0","message":"SUCCESS(PORT=2301)"}
# 发现 `config-provider-2301`的配置已更新
$ curl http://localhost:2302/profile/version
> {"code":200,"data":"v1.0","message":"SUCCESS(PORT=2302)"}
# 但是！`config-provider-2302`的配置仍未更新
```



### 6.定点通知

```bash
# 只能定点通知
$ curl -X POST http://localhost:2301/actuator/refresh

# 使用Spring Cloud Bus可以实现全局通知
```