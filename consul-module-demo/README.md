# HashiCorp Consul



### 1. Consul

  Consul 是分布式系统“一站式”解决方案，提供了服务注册、服务发现、健康检查、Key/Value 存储和多数据中心等功能。

- Consul对比Eureka

（1）Consul自带服务端，无需手动搭建模块；

（2）Consul实例名由 ${spring.application.name}-${server.port} 拼接指定，而Eureka需额外指定；

（3）Consul自带配置中心和消息总线；

（4）Consul支持K/V键值对数据缓存；

（5）Consul是CP原则，Eureka是AP原则；

（6）Consul是GO语言开发的，Eureka是Java语言开发的；



### 2. 搭建Consul服务端

- 下载

```bash
# https://www.consul.io/downloads
```

- 安装与启动

```bash
# 双击安装
# 开发模式启动（程序文件所在路径下执行）
$ consul agent -dev
```

- 访问

```bash
# http://localhost:8500
# 免密登录
```



### 3. 搭建Consul客户端

##### （1）搭建2个业务实例 Provider

- 建立 `consul-provider-2301` 子Module

- 在子pom中追加

```xml
<!-- Consul -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-consul-discovery</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableDiscoveryClient` 注解

```java
@SpringBootApplication
@EnableDiscoveryClient
public class ConsulProvider2301 {
    public static void main(String[] args) {
        SpringApplication.run(ConsulProvider2301.class, args);
    }
}
```

- 在application.yml追加配置

```yml
server:
  port: 2301

spring:
  application:
    name: consul-provider-service
  datasource:
    type: com.alibaba.druid.pool.DruidDataSource
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/cloudy?useUnicode=true&characterEncoding=utf-8&useSSL=false&allowPublicKeyRetrieval=true
    username: root
    password: 123456
  cloud:
    consul:
      host: localhost
      port: 8500
      discovery:
        service-name: ${spring.application.name}

mybatis-plus:
  configuration:
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl
  mapper-locations: classpath*:mapper/*.xml
  global-config:
    db-config:
      logic-not-delete-value: 0
      logic-delete-value: 1
```

- 编写Controller类

```java
@RestController
@Slf4j
public class ProviderController {

    @Resource
    private ProductService productService;

    @Value("${server.port}")
    private String serverPort;

    @GetMapping(value = "/product/get/{id}")
    public Results<Product> getOne(@PathVariable("id") Long id) {
        Product product = productService.getById(id);
        if (null == product) {
            return new Results<>(404, null, String.format("FAIL(PORT=%s)", serverPort));
        }
        return new Results<>(200, product, String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @PostMapping(value = "/product/new")
    public Results<Product> newOne(@RequestBody Product product) {
        productService.saveOrUpdate(product);
        return new Results<>(200, product, String.format("SUCCESS(PORT=%s)", serverPort));
    }
}
```

- 同样方法创建 `consul-provider-2302`



##### （2）搭建1个业务实例 Consumer

- 建立 `consul-consumer-2101` 子Module

- 在子pom中追加

```xml
<!-- Consul -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-consul-discovery</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableDiscoveryClient` 注解

```java
@SpringBootApplication
@EnableDiscoveryClient
public class ConsulConsumer2101 {
    public static void main(String[] args) {
        SpringApplication.run(ConsulConsumer2101.class, args);
    }
}
```

- 在application.yml追加配置

```yml
server:
  port: 2101

spring:
  application:
    name: consul-consumer-service
  cloud:
    consul:
      host: localhost
      port: 8500
      discovery:
        service-name: ${spring.application.name}
```

- 添加RestTemplate配置
```java
@Configuration
public class ApplicationContextConfig {

    @Bean
    @LoadBalanced // !important
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}
```

- 编写Controller类

```java
package com.alibaba.cloudy.controller;

import com.alibaba.cloudy.entities.Product;
import com.alibaba.cloudy.entities.Results;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@RequestMapping(value = "/consumer")
@RestController
@Slf4j
public class ConsumerController {

    private static final String PROVIDER_URL = "http://CONSUL-PROVIDER-SERVICE";

    @Resource
    private RestTemplate restTemplate;

    @Resource
    private DiscoveryClient discoveryClient;

    @GetMapping("/product/get/{id}")
    public Results getOne(@PathVariable("id") Long id) {
        return restTemplate.getForObject(PROVIDER_URL + "/product/get/" + id, Results.class);
    }

    @PostMapping("/product/new")
    public Results newOne(@RequestBody Product product) {
        return restTemplate.postForObject(PROVIDER_URL + "/product/new", product, Results.class);
    }

    @GetMapping(value = "/discovery")
    public Results discovery() {
        List<String> infos = new ArrayList<>();

        List<String> services = discoveryClient.getServices();
        for (String service : services) {
            List<ServiceInstance> instances = discoveryClient.getInstances(service);
            for (ServiceInstance instance : instances) {
                infos.add(instance.getServiceId() + " " + instance.getUri());
            }
        }
        return new Results(200, infos, "SUCCESS");
    }

}
```



### 4. 逐一启动各微服务

```bash
# 逐一启动 consul-server-8500.bat（注册中心）、consul-provider-2301（微服务实例 生产者）、consul-provider-2302（微服务实例 生产者）、consul-consumer-2101（微服务实例 消费者）
```



### 5. 测试用例

```bash
# 访问 http://localhost:8500/ui/dc1/services 查看注册列表
# Services 3 total
# ┣━ consul
# ┣━ consul-consumer-service
# ┗━ consul-provider-service
#     ┣━ consul-provider-service-2301
#     ┗━ consul-provider-service-2302

# 测试服务间的通信和负载均衡
$ curl http://localhost:2101/consumer/product/get/1
> {"code":200,"data":{"id":1,"name":"book","price":30.0,"count":2},"message":"SUCCESS(PORT=2301)"}
$ curl http://localhost:2101/consumer/product/get/1
> {"code":200,"data":{"id":1,"name":"book","price":30.0,"count":2},"message":"SUCCESS(PORT=2302)"}

# 测试服务发现（其中KANG为本机`设备名称`）
$ curl http://localhost:2101/consumer/discovery
> {"code":200,"data":["consul http://127.0.0.1:8300","consul-consumer-service http://KANG:2101","consul-provider-service http://KANG:2301","consul-provider-service http://KANG:2302"],"message":"SUCCESS"}



```

