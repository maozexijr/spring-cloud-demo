package com.alibaba.cloudy.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account implements Serializable {

    private Long id;
    private String name;
    private Double money;

    public Account(String name, Double money) {
        this.name = name;
        this.money = money;
    }

    public void reduce(Double money) {
        this.money -= money;
    }

}
