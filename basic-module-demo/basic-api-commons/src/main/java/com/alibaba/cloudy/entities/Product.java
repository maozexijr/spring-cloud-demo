package com.alibaba.cloudy.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product implements Serializable {

    private Long id;
    private String name;
    private Double price;
    private Integer count;

    public Product(String name, Double price, Integer count) {
        this.name = name;
        this.price = price;
        this.count = count;
    }

    public void reduce(Integer count) {
        this.count -= count;
    }

}
