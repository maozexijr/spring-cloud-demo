package com.alibaba.cloudy.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Results<T> {

    private int code;
    private T data;
    private String message;

    public boolean success() {
        if (200 == code) {
            return true;
        }
        return false;
    }

    /**
     * 有效载荷
     */
    public boolean payload() {
        if (!success()) {
            return false;
        }
        if (null == data) {
            return false;
        }
        return true;
    }

}
