# Basic



  Spring Cloud Alibaba 各组件实现示例 均依赖实体类 `Product` 和 `Account`，其数据库（MySQL）表结构如下：

- 建表SQL

```SQL
--- 
CREATE DATABASE `cloudy`;

---
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`
(
    `id`    bigint       NOT NULL AUTO_INCREMENT COMMENT '主键',
    `name`  varchar(100) NOT NULL UNIQUE COMMENT '品名',
    `price` decimal      NOT NULL DEFAULT 0 COMMENT '单价',
    `count` int          NOT NULL DEFAULT 0 COMMENT '库存',
    CHECK (`price` > 0 ),
    CHECK (`count` >= 0 ),
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;

INSERT INTO product(`name`, `price`, `count`)
VALUES ('book', 30, 2);

---
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account`
(
    `id`    bigint       NOT NULL AUTO_INCREMENT COMMENT '主键',
    `name`  varchar(100) NOT NULL UNIQUE COMMENT '用户',
    `money` decimal      NOT NULL DEFAULT 0 COMMENT '余额',
    CHECK (`money` >= 0 ),
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;

INSERT INTO account(`name`, `money`)
VALUES ('admin', 100);
```






- 结构示例

```
# ┏━━━━━━━━━━━━━━━┓       ┏━━━━━━━━━━━━━━━┓
# ┃ consumer1101 ┃  ->   ┃ provider1301 ┃ 
# ┗━━━━━━━━━━━━━━━┛       ┗━━━━━━━━━━━━━━━┛
```

