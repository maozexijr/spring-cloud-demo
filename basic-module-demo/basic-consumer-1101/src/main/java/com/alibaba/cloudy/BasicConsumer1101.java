package com.alibaba.cloudy;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
@Slf4j
public class BasicConsumer1101 {

    public static void main(String[] args) {
        SpringApplication.run(BasicConsumer1101.class, args);
        log.info("Ψ(￣∀￣)ΨΨ(￣∀￣)ΨΨ(￣∀￣)Ψ(づ￣ 3￣)づ(づ￣ 3￣)づ(づ￣ 3￣)づ(づ￣ 3￣)づ");
    }

}
