package com.alibaba.cloudy.controller;

import com.alibaba.cloudy.entities.Account;
import com.alibaba.cloudy.entities.Product;
import com.alibaba.cloudy.entities.Results;
import com.alibaba.cloudy.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;

@RequestMapping(value = "/consumer")
@RestController
@Slf4j
public class ConsumerController {

    private static final String PROVIDER_URL = "http://localhost:1301";

    @Resource
    private RestTemplate restTemplate;

    @Resource
    private AccountService accountService;

    @GetMapping("/account/list")
    public Results<List<Account>> listAll() {
        List<Account> list = accountService.list();
        return new Results<>(200, list, "SUCCESS");
    }

    @GetMapping("/product/get/{id}")
    public Results getOne(@PathVariable("id") Long id) {
        return restTemplate.getForObject(PROVIDER_URL + "/product/get/" + id, Results.class);
    }

    @PostMapping("/product/new")
    public Results newOne(@RequestBody Product product) {
        return restTemplate.postForObject(PROVIDER_URL + "/product/new", product, Results.class);
    }

}
