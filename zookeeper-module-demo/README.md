# Google Zookeeper



### 1. Zookeeper

  Zookeeper是Apache Hadoop的一个子项，主要由 `文件系统` 和 `监听通知` 两个部分组成，能作为分布式系统中的注册中心、配置中心使用。

- Zookeeper对比Eureka：

（1）Zookeeper自带服务端，无需手动搭建模块；



### 2. 搭建Zookeeper服务端

```bash
# 在/opt下下载并解压
$ wget https://downloads.apache.org/zookeeper/zookeeper-3.7.0/apache-zookeeper-3.7.0-bin.tar.gz
$ tar -zxvf apache-zookeeper-3.7.0-bin.tar.gz
# 在/opt/apache-zookeeper-3.7.0-bin/conf下备份并配置
$ cp zoo_sample.cfg zoo.cfg
$ vim zoo.cfg
> dataDir=/opt/apache-zookeeper-3.7.0-bin/data
> dataLogDir=/opt/apache-zookeeper-3.7.0-bin/logs

# 配置环境变量
$ vim /etc/profile
> # apache-zookeeper-3.7.0-bin
> export ZOOKEEPER_HOME=/opt/apache-zookeeper-3.7.0-bin
> export PAHT=$PATH:$ZOOKEEPER_HOME/bin
$ source /etc/profile # 刷新

# 在/opt/apache-zookeeper-3.7.0-bin/bin下以管理员权限启动
$ ./zkServer.sh start
$ ./zkServer.sh stop # 关闭
```



### 3. 搭建Zookeeper客户端

##### （1）搭建2个业务实例 Provider

- 建立 `zookeeper-provider-2301` 子Module

- 在子pom中追加

```xml
<!-- Zookeeper -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-zookeeper-discovery</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableDiscoveryClient` 注解

```java
@SpringBootApplication
@EnableDiscoveryClient
public class ZookeeperProvider2301 {
    public static void main(String[] args) {
        SpringApplication.run(ZookeeperProvider2301.class, args);
    }
}
```

- 在application.yml追加配置

```yml
server:
  port: 2301

spring:
  application:
    name: zookeeper-provider-service
  datasource:
    type: com.alibaba.druid.pool.DruidDataSource
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/cloudy?useUnicode=true&characterEncoding=utf-8&useSSL=false&allowPublicKeyRetrieval=true
    username: root
    password: 123456
  cloud:
    zookeeper:
      connect-string: 192.168.209.129:2181 # 默认端口2181
      session-timeout: 600000

mybatis-plus:
  configuration:
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl
  mapper-locations: classpath*:mapper/*.xml
  global-config:
    db-config:
      logic-not-delete-value: 0
      logic-delete-value: 1
```

- 编写Controller类

```java
@RestController
@Slf4j
public class ProviderController {

    @Resource
    private ProductService productService;

    @Value("${server.port}")
    private String serverPort;

    @GetMapping(value = "/product/get/{id}")
    public Results<Product> getOne(@PathVariable("id") Long id) {
        Product product = productService.getById(id);
        if (null == product) {
            return new Results<>(404, null, String.format("FAIL(PORT=%s)", serverPort));
        }
        return new Results<>(200, product, String.format("SUCCESS(PORT=%s)", serverPort));
    }

    @PostMapping(value = "/product/new")
    public Results<Product> newOne(@RequestBody Product product) {
        productService.saveOrUpdate(product);
        return new Results<>(200, product, String.format("SUCCESS(PORT=%s)", serverPort));
    }
}
```

- 同样方法创建 `zookeeper-provider-2302`



##### （2）搭建1个业务实例 Consumer

- 建立 `zookeeper-consumer-2101` 子Module

- 在子pom中追加

```xml
<!-- Zookeeper -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-zookeeper-discovery</artifactId>
</dependency>
```

- 在SpringBoot启动类追加 `@EnableDiscoveryClient` 注解

```java
@SpringBootApplication
@EnableDiscoveryClient
public class ZookeeperConsumer2101 {
    public static void main(String[] args) {
        SpringApplication.run(ZookeeperConsumer2101.class, args);
    }
}
```

- 在application.yml追加配置

```yml
server:
  port: 2101

spring:
  application:
    name: zookeeper-consumer-service
  cloud:
    zookeeper:
      connect-string: 192.168.209.129:2181 # 默认端口2181
      session-timeout: 600000
```

- 添加RestTemplate配置
```java
@Configuration
public class ApplicationContextConfig {

    @Bean
    @LoadBalanced // !important
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}
```

- 编写Controller层

```java
@RequestMapping(value = "/consumer")
@RestController
@Slf4j
public class ConsumerController {

    // !important: Service names are case-sensitive
    private static final String PROVIDER_URL = "http://zookeeper-provider-service";

    @Resource
    private RestTemplate restTemplate;

    @Resource
    private DiscoveryClient discoveryClient;

    @GetMapping("/product/get/{id}")
    public Results getOne(@PathVariable("id") Long id) {
        return restTemplate.getForObject(PROVIDER_URL + "/product/get/" + id, Results.class);
    }

    @PostMapping("/product/new")
    public Results newOne(@RequestBody Product product) {
        return restTemplate.postForObject(PROVIDER_URL + "/product/new", product, Results.class);
    }

    @GetMapping(value = "/discovery")
    public Results discovery() {
        List<String> infos = new ArrayList<>();

        List<String> services = discoveryClient.getServices();
        for (String service : services) {
            List<ServiceInstance> instances = discoveryClient.getInstances(service);
            for (ServiceInstance instance : instances) {
                infos.add(instance.getServiceId() + " " + instance.getUri());
            }
        }
        return new Results(200, infos, "SUCCESS");
    }

}
```

### 4. 逐一启动各微服务

```bash
# 逐一启动 zookeeper-server-2181（注册中心）、zookeeper-provider-2301（微服务实例 生产者）、zookeeper-provider-2302（微服务实例 生产者）、zookeeper-consumer-2101（微服务实例 消费者）
```



### 5. 测试用例

```bash
# 测试服务间的通信和负载均衡
$ curl http://localhost:2101/consumer/product/get/1
> {"code":200,"data":{"id":1,"name":"book","price":30.0,"count":2},"message":"SUCCESS(PORT=2301)"}
$ curl http://localhost:2101/consumer/product/get/1
> {"code":200,"data":{"id":1,"name":"book","price":30.0,"count":2},"message":"SUCCESS(PORT=2302)"}

# 测试服务发现（其中KANG为本机`设备名称`）
$ curl http://localhost:2101/consumer/discovery
> {"code":200,"data":["zookeeper-provider-service http://KANG:2301","zookeeper-provider-service http://KANG:2302","zookeeper-consumer-service http://KANG:2101"],"message":"SUCCESS"}
```
