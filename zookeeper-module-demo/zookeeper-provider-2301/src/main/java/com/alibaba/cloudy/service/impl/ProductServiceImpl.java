package com.alibaba.cloudy.service.impl;

import com.alibaba.cloudy.entities.Product;
import com.alibaba.cloudy.mapper.ProductMapper;
import com.alibaba.cloudy.service.ProductService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements ProductService {

    @Resource
    private ProductMapper productMapper;

    @Override
    public Product getByName(String name) {
        QueryWrapper<Product> query = new QueryWrapper<>();
        query.lambda().eq(Product::getName, name);
        return productMapper.selectOne(query);
    }

    @Override
    public Product reduce(String name, Integer count) {
        Product product = getByName(name);
        product.reduce(count);
        productMapper.updateById(product);
        return product;
    }

}
